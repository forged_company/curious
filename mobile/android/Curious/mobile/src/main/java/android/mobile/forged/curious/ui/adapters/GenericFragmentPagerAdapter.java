package android.mobile.forged.curious.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visitor15 on 2/24/15.
 */
public class GenericFragmentPagerAdapter extends FragmentStatePagerAdapter {

    public static final String ARGS = "args";

    private List<Fragment> fragments;

    public GenericFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
        init();
    }

    private void init() {
        fragments = new ArrayList<Fragment>();
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).toString();
    }

    public void setData(List<Fragment> fragments) {
        this.fragments.addAll(fragments);
    }

    public void addData(Fragment frag) {
        this.fragments.add(frag);
        this.notifyDataSetChanged();
    }

    public void removeData(int position) {
        this.fragments.remove(position);
        this.notifyDataSetChanged();
    }

    public void removeDataAfterIndex(int index) {
        for(int i = (index); i < this.fragments.size(); i++) {
            this.fragments.remove(i);
        }
        this.notifyDataSetChanged();
    }

    public void addAt(Fragment frag, int index, boolean purge) {

        if(index >= getCount()) {
            addData(frag);
            return;
        }

//        if(purge) {
//            for (int i = index; i < this.fragments.size(); i++) {
//                this.fragments.remove(i);
//            }
//            this.notifyDataSetChanged();
//        }

        this.fragments.add(index, frag);



        this.notifyDataSetChanged();
    }

    public void addAll(List<Fragment> frags) {
       this.fragments.addAll(frags);
        this.notifyDataSetChanged();
    }
}


















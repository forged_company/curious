package android.mobile.forged.curious.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.fragments.NavigationDrawerFragment;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademy;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademyClientAuthenticator;
import android.mobile.forged.curious.integrated.khan_academy.fragments.KhanAcademyProfileFragment;
import android.mobile.forged.curious.integrated.khan_academy.fragments.KhanAcademyTabbedTopicBrowsingFragment;
import android.mobile.forged.curious.integrated.khan_academy.fragments.KhanAcademyTopicBrowser;
import android.mobile.forged.curious.integrated.khan_academy.fragments.KhanAcademyTopicBrowserFragment;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.mobile.forged.curious.user.User;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.google.common.base.Strings;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private KhanAcademy khanAcademy;

    private KhanAcademyClientAuthenticator khanAcademyClientAuthenticator;

    boolean firstBoot = true;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
//        getSupportActionBar().hide();
//        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background_alpha));
        if(khanAcademy == null) {
            khanAcademy = new KhanAcademy(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
//                    final FragmentManager fragmentManager = getSupportFragmentManager();
////                    KhanAcademyTopicBrowser frag = new KhanAcademyTopicBrowser();
//                    KhanAcademyProfileFragment frag = new KhanAcademyProfileFragment();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.container, frag)
//                            .commit();
                }

                @Override
                public void handleServiceMessage(Message msg) {

                }
            });
        }

        if(khanAcademyClientAuthenticator == null) {
            khanAcademyClientAuthenticator = new KhanAcademyClientAuthenticator(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
                    firstBoot = false;
                }

                @Override
                public void handleServiceMessage(Message msg) {

                }
            });
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        if(!firstBoot) {
            khanAcademyClientAuthenticator.authenticateUser(new KhanAcademyUser() {}, new DataCallback<Bundle>() {
                @Override
                public void receiveResults(Bundle results) {
                    if(!Strings.isNullOrEmpty(results.getString("token"))) {
                        Log.d(getClass().getName(), "GOT AUTH RESULTS: " + results.getString("token"));
                        Toast.makeText(MainActivity.this, "GOT TOKEN: " + results.getString("token"), Toast.LENGTH_LONG).show();
                        khanAcademy.getUser("nickc.dev", new DataCallback<KhanAcademyUser>() {
                            @Override
                            public void receiveResults(KhanAcademyUser user) {
                                Log.d(getClass().getName(), "GOT USER: " + user.getDisplayName());
                                Toast.makeText(getApplicationContext(), "GOT USER: " + user.getDisplayName(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            });
        }

        // update the main content by replacing fragments
//        final FragmentManager fragmentManager = getSupportFragmentManager();


//        if(khanAcademy != null) {
//            khanAcademy.getAllMathTopics(new DataCallback<KhanTopic>() {
//                @Override
//                public void receiveResults(KhanTopic results) {
//                    Log.d("TAG", "GOT TOPICS: " + results.getId());
//                    Bundle args = new Bundle();
//                    args.putSerializable("topic", results);
//                    KhanAcademyRootTopicBrowserFragment frag = new KhanAcademyRootTopicBrowserFragment();
//                    frag.setArguments(args);
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.container, frag)
//                            .addToBackStack(null)
//                            .commit();
//                }
//            });
//        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}

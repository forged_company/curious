package android.mobile.forged.curious.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicDataBuilder;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.storage.KhanDBHelper;
import android.mobile.forged.curious.user.User;

import com.google.common.base.Optional;

import java.util.Map;

/**
 * Created by visitor15 on 2/17/15.
 */
public class DBManager extends SimpleDatabaseManager {

    public DBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        upgradeDatabase(db, newVersion);
    }

    @Override
    protected void createDatabase(SQLiteDatabase db, int version) {
        db.execSQL(DBManagerHelper.CREATE_DEFAULT_DATABASE);
    }

    @Override
    protected void upgradeDatabase(SQLiteDatabase db, int newVersion) {

    }

    public Optional getItem(String key) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DBManagerHelper.DBEntries._ID,
                DBManagerHelper.DBEntries.COLUMN_PREFERENCE_ID,
                DBManagerHelper.DBEntries.COLUMN_PREFERENCE_VALUE,
                DBManagerHelper.DBEntries.COLUMN_PREFERENCE_USER_SETTING,
                DBManagerHelper.DBEntries.COLUMN_PREFERENCE_DEF_VALUE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = DBManagerHelper.DBEntries.COLUMN_PREFERENCE_ID + " DESC";

        String selection = DBManagerHelper.DBEntries.COLUMN_PREFERENCE_ID + " = ?";
        String[] selectionArgs = {key};
        Cursor c = db.query(
                DBManagerHelper.DBEntries.TABLE_NAME_TOPIC,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if (c.moveToFirst()) {
            String value = c.getString(c.getColumnIndexOrThrow(DBManagerHelper.DBEntries.COLUMN_PREFERENCE_VALUE));
            return Optional.fromNullable(value);
        }
        return Optional.absent();
    }

    public long insertItem(String key, String value) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        // TODO video topics are returning a number as an idea. This needs to be handled.
        values.put(DBManagerHelper.DBEntries.COLUMN_PREFERENCE_ID, key);
        values.put(DBManagerHelper.DBEntries.COLUMN_PREFERENCE_VALUE, value);
        values.put(DBManagerHelper.DBEntries.COLUMN_PREFERENCE_USER_SETTING, value);

        // Insert the new row, returning the primary key value of the new row
        return db.insert(
                DBManagerHelper.DBEntries.TABLE_NAME_TOPIC,
                null,
                values);
    }

    // TODO These need to go!
    @Override
    public long persistTopic(Topic topic) {
        return 0;
    }

    @Override
    public long persistUser(User user) {
        return 0;
    }

    @Override
    public Topic findTopicByTopicID(String topicId) {
        return null;
    }

    @Override
    public long persistAuthentication(User currentUser, Map<String, String> authResults) {
        return 0;
    }

    @Override
    public String getRefreshTokenForUser(User currentUser) {
        return "";
    }

    @Override
    public String getOauthVerifierForUser(User currentUser) {
        return "";
    }

    @Override
    public String getSignatureSigningKey(User currentUser) { return ""; }

    @Override
    public String getAccessTokenForUser(User currentUser) { return ""; }

    @Override
    public Optional<? extends User> getUser(String userName) { return Optional.absent(); }

    @Override
    public String getAccessTokenSignatureSigningKey(User currentUser) { return ""; }
}

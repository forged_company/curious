package android.mobile.forged.curious.data;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;

/**
 * Created by visitor15 on 2/18/15.
 */
public abstract class JsonData<T> implements Serializable {

    private String _jsonString = "";

    private String _jsonSource = "";

    public JsonData(final String jsonSource, final String jsonString) {
        this._jsonString = jsonString;
        this._jsonSource = jsonSource;
    }

    public JsonData(final String jsonData) {
        this._jsonString = jsonData;
    }

    public String getJsonString() {
        return _jsonString.toString();
    }

    public String getJsonSource() {
        return _jsonSource.toString();
    }

    public abstract Optional<T> resolve();
}

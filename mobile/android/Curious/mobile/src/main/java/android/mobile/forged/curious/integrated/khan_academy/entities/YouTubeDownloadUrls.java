package android.mobile.forged.curious.integrated.khan_academy.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by visitor15 on 2/18/15.
 */
public class YouTubeDownloadUrls {
    @SerializedName("mp4")
    protected String mp4 = "";
    @SerializedName("png")
    protected String png = "";
    @SerializedName("m3u8")
    protected String m3u8 = "";

    public YouTubeDownloadUrls() {
    }

    public String getMp4() {
        return mp4;
    }

    public String getPng() {
        return png;
    }

    public String getM3u8() {
        return m3u8;
    }
}

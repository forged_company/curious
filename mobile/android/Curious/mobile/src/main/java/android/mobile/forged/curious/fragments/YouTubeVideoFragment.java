package android.mobile.forged.curious.fragments;

import android.content.res.Configuration;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademy;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

/**
 * Created by nchampagne on 2/19/15.
 */
public class YouTubeVideoFragment extends Fragment {
    private Topic topic;

    private View rootView;

    private YouTubePlayerFragment youTubePlayerFragment;

    private KhanAcademy khanAcademy;

    private TextView metaTitleText;

    private TextView metaDescriptionText;

    private TextView metaDurationText;

    public YouTubePlayer.Provider provider;

    public YouTubePlayer youTubePlayer;

    public YouTubeVideoFragment() {
        khanAcademy = new KhanAcademy();
    }

    private void initYouTubePlayer() {
        youTubePlayerFragment = (com.google.android.youtube.player.YouTubePlayerFragment) getActivity().getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.initialize("AIzaSyAjJL1s102VUxvrHMUIJRjlmYDdbMx2MyQ", new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
                YouTubeVideoFragment.this.provider = provider;
                YouTubeVideoFragment.this.youTubePlayer = youTubePlayer;

                if (!wasRestored) {
                    youTubePlayer.cueVideo(topic.getYouTubeId());
                }
            }

            @Override
            public void onInitializationFailure(com.google.android.youtube.player.YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                youTubeInitializationResult.getErrorDialog(getActivity(), 0);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.youtube_fragment, container, false);
        metaTitleText = (TextView) rootView.findViewById(R.id.textView_title);
        metaDescriptionText = (TextView) rootView.findViewById(R.id.textView_description);
        metaDurationText = (TextView) rootView.findViewById(R.id.textView_duration);
        metaTitleText.setText(topic.getTopicData().getDisplayName());

        initKhanService();

        return rootView;
    }

    private void initKhanService() {
        if(topic == null) {
            khanAcademy = new KhanAcademy(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
                    getVideoData();
                }

                @Override
                public void handleServiceMessage(Message msg) {
                    Log.d(getClass().getName(), "GOT SERVICE MESSAGE!");
                }
            });
        }
        else {
            initWithTopic((KhanTopic) topic);
        }
    }

    private void extractArguments() {
        Bundle b = getArguments();
        if(b != null) {
            if(b.containsKey("topic")) {
                topic = (KhanTopic) getArguments().getSerializable("topic");
            }
        }
    }

    private void getVideoData() {
        if(topic == null) {
            khanAcademy.getVideoDataForTopic((KhanTopic) topic, new DataCallback<KhanTopic>() {

                @Override
                public void receiveResults(KhanTopic results) {
                    initWithTopic(results);
                }
            });
        }
    }

    private void initWithTopic(KhanTopic t) {
        this.topic = t;
        metaDescriptionText.setText(topic.getTopicData().getDescription());
        initYouTubePlayer();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       YouTubePlayerFragment f = (YouTubePlayerFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.youtube_fragment);
        if (f != null)
            getActivity().getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
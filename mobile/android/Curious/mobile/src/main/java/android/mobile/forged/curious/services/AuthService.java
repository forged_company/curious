package android.mobile.forged.curious.services;

import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.auth.SimpleAuthenticator;
import android.mobile.forged.curious.user.User;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by visitor15 on 3/2/15.
 */
public abstract class AuthService extends LocalService implements SimpleAuthenticator {

    @Override
    protected void handleWorkerMessage(Message msg) {
        handleMessage(Message.obtain(msg));
    }

    @Override
    public abstract Bundle authenticate(User user);

    private void handleMessage(Message msg) {
        switch(msg.what) {
            case Services.REQUEST_AUTHENTICATION: {
                authenticateInternal(Message.obtain(msg));
                break;
            }
            case Services.REQUEST_PERSIST_AUTHENTICATION: {
                persistAuthenticationInternal(msg.getData());
                break;
            }
            default :
                sendAuthFailure();
        }
    }

    private void sendAuthFailure() {

    }

    private void persistAuthenticationInternal(Bundle data) {
        Map<String, String> authResults = new HashMap<String, String>();
        String oauthSecret = "";
        String oauthToken = "";
        String oauthVerifier = "";
        if(data.containsKey("oauth_secret")) {
            oauthSecret = data.getString("oauth_secret");
        }
        if(data.containsKey("oauth_token")) {
            oauthToken = data.getString("oauth_token");
        }
        if(data.containsKey("oauth_verifier")) {
            oauthVerifier = data.getString("oauth_verifier");
        }

        authResults.put("oauth_secret", oauthSecret);
        authResults.put("oauth_token", oauthToken);
        authResults.put("oauth_verifier", oauthVerifier);

        //TODO Default database or auth-specific db?
        Curious.getDefaultDatabase().persistAuthentication(Curious.getCurrentUser(), authResults);
    }

    private void authenticateInternal(Message msg) {
        Bundle b = authenticate(Curious.getUserService().getCurrentUser());
        msg.getData().putBundle("result_set", b);
        handleResponse(Message.obtain(msg));
    }

    private void handleResponse(Message msg) {
        try {
            msg.replyTo.send(Message.obtain(msg));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        killCurrentWorkerThread();
    }

    public abstract String makeAuthenticatedGETRequest(User user, String url);

    public static final class Services {
        public static final int REQUEST_AUTHENTICATION = 0;
        public static final int REQUEST_PERSIST_AUTHENTICATION = 1;
    }
}

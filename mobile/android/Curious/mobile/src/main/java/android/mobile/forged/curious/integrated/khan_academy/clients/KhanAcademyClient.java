package android.mobile.forged.curious.integrated.khan_academy.clients;

import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.services.clients.SimpleClient;

import java.util.List;

/**
 * Created by visitor15 on 2/19/15.
 */
public interface KhanAcademyClient extends SimpleClient {

    public void getNewAndNoteworthyTopics(final DataCallback<KhanTopic> c);
    public void getAllMathTopics(final DataCallback<KhanTopic> c);
    public void getAllScienceTopics(final DataCallback<KhanTopic> c);
    public void getAllEconomicsAndFinanceTopics(final DataCallback<KhanTopic> c);
    public void getAllArtsAndHumanitiesTopics(final DataCallback<KhanTopic> c);
    public void getAllComputingTopics(final DataCallback<KhanTopic> c);
    public void getAllTestPrepTopics(final DataCallback<KhanTopic> c);
    public void getAllPartnerContentTopics(final DataCallback<KhanTopic> c);
    public void getAllTalksAndInterviews(final DataCallback<KhanTopic> c);
//    public void getVideosForTopic(final Topic topic, final DataCallback<Topic> c);
//    public void getSubTopics(final Topic topic, final DataCallback<? extends Topic> c);
//    public void getVideoSubTopics(Topic topic, DataCallback<? extends Topic> c);

    public void getRootTopics(final DataCallback<List<KhanTopic>> c);
    public void getUser(final String userName, final DataCallback<KhanAcademyUser> c);
}

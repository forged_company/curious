package android.mobile.forged.curious.services;

import android.mobile.forged.curious.user.User;

/**
 * Created by visitor15 on 3/2/15.
 */
public interface SimpleUserService extends SimpleService {

    public User getCurrentUser();
}

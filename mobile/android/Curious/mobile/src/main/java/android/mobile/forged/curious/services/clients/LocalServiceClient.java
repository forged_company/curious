package android.mobile.forged.curious.services.clients;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.services.LocalService;
import android.mobile.forged.curious.tasks.Task;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;

/**
 * Created by visitor15 on 2/19/15.
 */
public abstract class LocalServiceClient {
    protected transient Messenger _messenger;

    protected boolean _isBound;

    protected transient ServiceConnection _serviceConnection;

    protected transient SimpleClientCallback _callback;

    protected ServiceHandler _serviceHandler;

    protected Messenger _mCallbackMessenger = new Messenger(new ServiceHandler());

    public LocalServiceClient() {
        _callback = new SimpleClientCallback() {
            @Override
            public void onServiceConnected() {
                /* Nothing - we own this service connection. */
            }

            @Override
            public void handleServiceMessage(Message msg) {
                onHandleMessage(msg);
            }
        };
    }

    public LocalServiceClient(SimpleClientCallback callback, Class c) {
        _callback = callback;
        initializeService(c);
    }

    protected void initializeService(Class c) {
        _serviceHandler = new ServiceHandler(Looper.myLooper());
        Curious.getContext().bindService(new Intent(Curious.getContext(),
                        c),
                createServiceConnection(),
                Context.BIND_AUTO_CREATE);
    }

    protected ServiceConnection createServiceConnection() {
        _serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                _isBound = true;
                _messenger = new Messenger(service);
                _callback.onServiceConnected();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                _isBound = false;
            }
        };

        return _serviceConnection;
    }

    public void doTask(Task t) {
        Bundle b = new Bundle();
        b.putSerializable("task", t);
        Message msg = new Message();
        msg.setData(b);
        msg.what = LocalService.Services.DO_TASK;

        try {
            _messenger.send(msg);
        } catch (Exception e) {
        }
    }

    protected abstract void onHandleMessage(final Message msg);

    protected class ServiceHandler extends Handler {

        protected ServiceHandler() {}

        protected ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            synchronized (this) {
                onHandleMessage(Message.obtain(msg));
            }
        }
    }
}
package android.mobile.forged.curious.fragments;

import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicData;
import android.mobile.forged.curious.ui.adapters.GenericListAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by visitor15 on 2/20/15.
 */
public class TopicBrowserFragment extends Fragment {

    private View rootView;

    private Topic topic;

    private ListView listView;

    private GenericListAdapter<KhanTopicData> listViewAdapter;

    private LayoutInflater layoutInflater;

    public TopicBrowserFragment() {
    }

    private void extractArguments() {
        Bundle b = getArguments();
        if(b != null) {
            if(b.containsKey("topic")) {
                topic = (Topic) getArguments().getSerializable("topic");
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = inflater;
        rootView = layoutInflater.inflate(R.layout.topic_browser_fragment_layout, container, false);

        initUIElements();

        return rootView;
    }

    private void initUIElements() {
        if(listView == null) {
            listView = (ListView) rootView.findViewById(R.id.listView);
            listViewAdapter = new GenericListAdapter<KhanTopicData>((List<KhanTopicData>) topic.getChildren()) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    if (convertView == null) {
                        convertView = layoutInflater.inflate(R.layout.root_topic_item_layout, parent, false);
                    }

                    ((TextView) convertView.findViewById(R.id.textView_title)).setText(getData().get(position).getDisplayName());
                    ((TextView) convertView.findViewById(R.id.textView_description)).setText(getData().get(position).getDescription());
                    convertView.setBackgroundColor(getData().get(position).getBackgroundColor());
                    ((GradientDrawable) convertView.findViewById(R.id.circle_shape).getBackground()).setColor(Curious.getContext().getResources().getColor(getData().get(position).getBackgroundColor()));

                    return convertView;
                }
            };

            setAdapterClickListener();
            listView.setAdapter(listViewAdapter);
        }
    }

    private void setAdapterClickListener() {
        if(listView != null) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CardView cardView = (CardView) view.findViewById(R.id.card_view);
                    if(cardView.getCardElevation() == 0f) {
                        cardView.setCardElevation(4f);
                    }
                    else {
                        cardView.setCardElevation(0f);
                    }
                }
            });
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

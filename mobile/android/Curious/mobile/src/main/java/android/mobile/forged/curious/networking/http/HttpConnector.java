package android.mobile.forged.curious.networking.http;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Locale;

import javax.net.ssl.SSLProtocolException;

/**
 * Created by visitor15 on 2/18/15.
 */
public class HttpConnector {

    private URL mUrl;

    public HttpConnector() {}

    public String postForResponse(String url) {
        Log.d(getClass().getName(), "SENDING REQUEST TO: " + url);
        HttpURLConnection httpConnection = null;
        String response = "";
        try {
            mUrl = new URL(url);
            httpConnection = (HttpURLConnection) mUrl.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setRequestProperty("Content-Type", "application/json");
            httpConnection.setRequestProperty("Content-Language", Locale.getDefault().getISO3Language());

            httpConnection.setUseCaches(false);
            httpConnection.setDoInput(true);

            InputStream inStream = httpConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

            String line = "";
            StringBuilder strBuilder = new StringBuilder(line);
            while((line = reader.readLine()) != null) {
                strBuilder.append(line);
            }

            response = strBuilder.toString();

            reader.close();
        } catch (SSLProtocolException e) {
            e.printStackTrace();
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(httpConnection != null) {
                httpConnection.disconnect();
            }
        }

        System.out.println(response);

        return response;
    }
}
package android.mobile.forged.curious.entities;

import android.graphics.Color;
import android.mobile.forged.curious.data.TopicData;

import java.io.Serializable;
import java.util.List;

/**
 * Created by visitor15 on 2/18/15.
 */
public interface Topic extends Serializable{

    public TopicData getTopicData();

    public String getId();

    public String getYouTubeId();

    public List<? extends TopicData> getChildren();

    public String getUrl();

    public String getKind();

    public String toString();
}

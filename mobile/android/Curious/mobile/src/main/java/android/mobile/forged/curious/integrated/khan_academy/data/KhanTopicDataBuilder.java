package android.mobile.forged.curious.integrated.khan_academy.data;

import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.data.TopicDataBuilder;

import java.util.List;

/**
 * Created by visitor15 on 2/18/15.
 */
public class KhanTopicDataBuilder implements TopicDataBuilder {
    @Override
    public JsonData build(String jsonSource, String jsonString) {
        // TODO Using ONLY KhanTopic, but this might need to be reevaluated.
        return buildInternal(jsonSource, jsonString);
    }

    @Override
    public JsonData buildFromJsonString(String jsonData) {
        return buildInternal(jsonData);
    }

    @Override
    public JsonData<List<? extends TopicData>> buildForList(String jsonSource, String jsonString) {
        return buildInternalForList(jsonSource, jsonString);
    }

    private JsonData<KhanTopicData> buildInternal(final String jsonSource, final String jsonString) {
        return filterForOnlyVideosAndTopics(new KhanTopicData(jsonSource, jsonString));
    }

    private JsonData<KhanTopicData> buildInternal(final String jsonData) {
        return filterForOnlyVideosAndTopics(new KhanTopicData(jsonData));
    }

    private JsonData<List<? extends TopicData>> buildInternalForList(final String jsonSource, final String jsonString) {
        // I can't believe I have to do it like this.
        return null;
    }

    private JsonData<KhanTopicData> filterForOnlyVideosAndTopics(KhanTopicData topicData) {
        List<KhanTopicData> subTopics = topicData.children;
        if(subTopics != null) {
            for (KhanTopicData data : subTopics) {
                if (!(data.kind.equalsIgnoreCase("video")) || !(data.kind.equalsIgnoreCase("topic"))) {
                    subTopics.remove(data);
                }
            }
        }
        return topicData;
    }
}

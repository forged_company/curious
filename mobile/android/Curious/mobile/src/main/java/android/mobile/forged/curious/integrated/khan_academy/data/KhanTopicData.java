package android.mobile.forged.curious.integrated.khan_academy.data;

import android.graphics.Color;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.integrated.khan_academy.entities.YouTubeDownloadUrls;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by visitor15 on 2/18/15.
 */
public class KhanTopicData extends JsonData<KhanTopicData> implements TopicData {

    private int primaryColor = R.color.material_blue_grey_800;
    private int secondaryColor = android.R.color.white;
    private int backgroundColor = android.R.color.holo_orange_light;
    private int primaryVideoColor = android.R.color.holo_blue_light;

    public String author_key = "";

    @SerializedName("parent_domain")
    private String parent_domain = "";

    @SerializedName("children")
    public List<KhanTopicData> children;

    @SerializedName("child_data")
    public List<KhanTopicData> childrenMeta;

    @SerializedName("creation_date")
    public String creation_date = "";

    @SerializedName("description")
    public String description = "";

    @SerializedName("domain_slug")
    public String domain_slug = "";

    @SerializedName("do_not_publish")
    public String do_not_publish = "";

    @SerializedName("edit_slug")
    public String edit_slug = "";

    @SerializedName("extended_slug")
    public String extended_slug = "";

    @SerializedName("gplus_url")
    public String gplus_url = "";

    @SerializedName("has_user_authored_content_types")
    public String has_user_authored_content_types = "";

    @SerializedName("hide")
    public String hide = "";

    @SerializedName("icon_src")
    public String icon_src = "";

    @SerializedName("id")
    public String id = "";

    @SerializedName("internal_id")
    public String internal_id = "";

    @SerializedName("slug")
    public String slug = "";

    @SerializedName("ka_url")
    public String ka_url = "";

    @SerializedName("key")
    public String key = "";

    @SerializedName("kind")
    public String kind = "";

    @SerializedName("node_slug")
    public String node_slug = "";

    @SerializedName("old_key_name")
    public String old_key_name = "";

    @SerializedName("relative_url")
    public String relative_url = "";

    @SerializedName("standalone_title")
    public String standalone_title = "";

    @SerializedName("tags")
    public List<String> tags;

    @SerializedName("title")
    public String title = "";

    @SerializedName("topics_page_url")
    public String topics_page_url = "";

    @SerializedName("translated_title")
    public String translated_title = "";

    @SerializedName("translated_standalone_title")
    public String translated_standalone_title = "";

    @SerializedName("url")
    public String url = "";

    @SerializedName("web_url")
    public String web_url = "";

    @SerializedName("download_urls")
    public YouTubeDownloadUrls download_urls;

    @SerializedName("translated_youtube_id")
    public String translated_youtube_id = "";

    @SerializedName("youtube_id")
    public String youtube_id = "";

    @SerializedName("translated_description")
    public String translated_description = "";

    @SerializedName("image_url")
    public String image_url = "";

    @SerializedName("keywords")
    public String keywords = "";

    public KhanTopicData(String jsonSource, String jsonString) {
        super(jsonSource, jsonString);
        initColors();
    }

    public KhanTopicData(String jsonData) {
        super(jsonData);
        initColors();
    }

    private void initColors() {
        primaryColor = R.color.material_blue_grey_800;
        secondaryColor = android.R.color.white;
        backgroundColor = android.R.color.holo_orange_light;

    }

    /**************************************************************************
     * METHODS
     *************************************************************************/
    @Override
    public String getDisplayName() {
        return title.toString();
    }

    @Override
    public String getDescription() {
        return (description != null) ? description.toString() : "";
    }

    @Override
    public int getPrimaryColor() {
        return primaryColor;
    }

    @Override
    public int getSecondaryColor() {
        return secondaryColor;
    }

    @Override
    public int getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public int getPrimaryVideoColor() {
        return primaryVideoColor;
    }

    @Override
    public void setPrimaryColor(int colorResource) {
        this.primaryColor = colorResource;
    }

    @Override
    public void setSecondaryColor(int colorResource) {
        this.secondaryColor = colorResource;
    }

    @Override
    public void setBackgroundColor(int colorResource) {
        this.backgroundColor = colorResource;
    }

    @Override
    public void setPrimaryVideoColor(int colorResource) {
        this.primaryVideoColor = colorResource;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDomain() {
        return (domain_slug != null) ? domain_slug : parent_domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain_slug = domain;
    }

    @Override
    public void setParentDomain(String parentDomain) {
        this.parent_domain = parentDomain;
    }

    @Override
    public Optional<KhanTopicData> resolve() {
        KhanTopicData element;
        Gson gson = new GsonBuilder().create();
        try {
            element = gson.fromJson(getJsonString(), new TypeToken<KhanTopicData>() {
            }.getType());
        } catch (NullPointerException e) {
            element = this;
        }
        return Optional.fromNullable(element);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(KhanTopicData.this, new TypeToken<KhanTopicData>() {
        }.getType());
    }
}

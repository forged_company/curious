package android.mobile.forged.curious.integrated.khan_academy.data;

import android.mobile.forged.curious.user.LocalUserData;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Map;

/**
 * Created by visitor15 on 3/3/15.
 */
public class KhanUserData extends LocalUserData<KhanUserData> {

    @SerializedName("spawned_by")
    private String spawned_by = "";

    @SerializedName("students_count_temp")
    private String students_count_temp = "";

    @SerializedName("can_record_tutorial")
    private String can_record_tutorial = "";

    @SerializedName("is_demo")
    private boolean is_demo = false;

    @SerializedName("is_parent")
    private boolean is_parent = false;

    @SerializedName("creator")
    private boolean creator = false;

    @SerializedName("total_seconds_watched")
    private int total_seconds_watched = 0;

    @SerializedName("tos_accepted")
    private int tos_accepted = 0;

    @SerializedName("joined")
    private String joined = "";

    @SerializedName("key_email")
    private String key_email = "";

    @SerializedName("has_changed_avatar")
    private boolean has_changed_avatar = false;

    @SerializedName("is_pre_phantom")
    private boolean is_pre_phantom = false;

    @SerializedName("developer")
    private boolean developer = false;

    @SerializedName("first_visit")
    private String first_visit = "";

    @SerializedName("user_id")
    private String user_id = "";

    @SerializedName("may_be_child")
    private boolean may_be_child = false;

    @SerializedName("title")
    private String title = "";

    @SerializedName("all_proficient_exercises")
    private List<String> all_proficient_exercises;

    @SerializedName("students_count")
    private int students_count = 0;

    @SerializedName("student_summary")
    private StudentSummary student_summary;

    @SerializedName("url_segment")
    private String url_segment = "";

    @SerializedName("capabilities")
    private List<String> capabilities;

    @SerializedName("UNKNOWN_CLASSROOM_STATUS")
    private int UNKNOWN_CLASSROOM_STATUS = 0;

    @SerializedName("profile_root")
    private String profile_root = "";

    @SerializedName("avatar_url")
    private String avatar_url = "";

    @SerializedName("coworkers")
    private List<String> coworkers;

    @SerializedName("follow_requires_approval")
    private boolean follow_requires_approval;

    @SerializedName("discussion_banned")
    private boolean discussion_banned = false;

    @SerializedName("CLASSROOM_USER")
    private int CLASSROOM_USER = 0;

    @SerializedName("homepage")
    private String homepage = "";

    @SerializedName("username")
    private String username = "";

    @SerializedName("last_badge_review")
    private String last_badge_review = "";

    @SerializedName("is_midsignup_phantom")
    private boolean is_midsignup_phantom = false;

    @SerializedName("registration_date")
    private String registration_date = "";

    @SerializedName("points")
    private int points = 0;

    @SerializedName("coaches")
    private List<String> coaches;

    @SerializedName("last_modified_as_mapreduce_epoch")
    private long last_modified_as_mapreduce_epoch = 0;

    @SerializedName("is_child_account")
    private boolean is_child_account = false;

    @SerializedName("user_language")
    private String user_language = "";

    @SerializedName("phantom_creation_date")
    private String phantom_creation_date = "";

    @SerializedName("uservideocss_version")
    private int uservideocss_version = 0;

    @SerializedName("start_consecutive_activity_date")
    private String start_consecutive_activity_date = "";

    @SerializedName("nickname")
    private String nickname = "";

    @SerializedName("teacher")
    private boolean teacher = false;

    @SerializedName("is_inferred_teacher")
    private boolean is_inferred_teacher = false;

    @SerializedName("user_homepage")
    private String user_homepage = "";

    @SerializedName("publisher")
    private boolean publisher = false;

    @SerializedName("kind")
    private String kind = "";

    @SerializedName("is_inferred_parent")
    private boolean is_inferred_parent = false;

    @SerializedName("unverified_registration_date")
    private String unverified_registration_date = "";

    @SerializedName("is_moderator_or_developer")
    private boolean is_moderator_or_developer = false;

    @SerializedName("last_activity")
    private String last_activity = "";

    @SerializedName("is_phantom")
    private boolean is_phantom = false;

    @SerializedName("proficient_exercises")
    private List<String> proficient_exercises;

    @SerializedName("curator")
    private boolean curator = false;

    @SerializedName("restricted_domain")
    private String restricted_domain = "";

    @SerializedName("gae_bingo_identity")
    private String gae_bingo_identity = "";

    @SerializedName("homepage_url")
    private String homepage_url = "";

    @SerializedName("classroom_user_status")
    private int classroom_user_status = 0;

    @SerializedName("NOT_CLASSROOM_USER")
    private int NOT_CLASSROOM_USER = 0;

    @SerializedName("featured_scratchpads")
    private List<String> featured_scratchpads;

    @SerializedName("userprogresscache_version")
    private int userprogresscache_version = 0;

    @SerializedName("kaid")
    private String kaid = "";

    @SerializedName("badge_counts")
    private Map<String, Integer> badge_counts;

    public String getSpawned_by() {
        return spawned_by;
    }

    public String getStudents_count_temp() {
        return students_count_temp;
    }

    public String getCan_record_tutorial() {
        return can_record_tutorial;
    }

    public boolean isIs_demo() {
        return is_demo;
    }

    public boolean isIs_parent() {
        return is_parent;
    }

    public boolean isCreator() {
        return creator;
    }

    public int getTotal_seconds_watched() {
        return total_seconds_watched;
    }

    public int getTos_accepted() {
        return tos_accepted;
    }

    public String getJoined() {
        return joined;
    }

    public String getKey_email() {
        return key_email;
    }

    public boolean isHas_changed_avatar() {
        return has_changed_avatar;
    }

    public boolean isIs_pre_phantom() {
        return is_pre_phantom;
    }

    public boolean isDeveloper() {
        return developer;
    }

    public String getFirst_visit() {
        return first_visit;
    }

    public String getUser_id() {
        return user_id;
    }

    public boolean isMay_be_child() {
        return may_be_child;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getAll_proficient_exercises() {
        return all_proficient_exercises;
    }

    public int getStudents_count() {
        return students_count;
    }

    public StudentSummary getStudent_summary() {
        return student_summary;
    }

    public String getUrl_segment() {
        return url_segment;
    }

    public List<String> getCapabilities() {
        return capabilities;
    }

    public int getUNKNOWN_CLASSROOM_STATUS() {
        return UNKNOWN_CLASSROOM_STATUS;
    }

    public String getProfile_root() {
        return profile_root;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public List<String> getCoworkers() {
        return coworkers;
    }

    public boolean isFollow_requires_approval() {
        return follow_requires_approval;
    }

    public boolean isDiscussion_banned() {
        return discussion_banned;
    }

    public int getCLASSROOM_USER() {
        return CLASSROOM_USER;
    }

    public String getHomepage() {
        return homepage;
    }

    public String getLast_badge_review() {
        return last_badge_review;
    }

    public boolean isIs_midsignup_phantom() {
        return is_midsignup_phantom;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public int getPoints() {
        return points;
    }

    public List<String> getCoaches() {
        return coaches;
    }

    public long getLast_modified_as_mapreduce_epoch() {
        return last_modified_as_mapreduce_epoch;
    }

    public boolean isIs_child_account() {
        return is_child_account;
    }

    public String getUser_language() {
        return user_language;
    }

    public String getPhantom_creation_date() {
        return phantom_creation_date;
    }

    public int getUservideocss_version() {
        return uservideocss_version;
    }

    public String getStart_consecutive_activity_date() {
        return start_consecutive_activity_date;
    }

    public String getNickname() {
        return nickname;
    }

    public boolean isTeacher() {
        return teacher;
    }

    public boolean isIs_inferred_teacher() {
        return is_inferred_teacher;
    }

    public String getUser_homepage() {
        return user_homepage;
    }

    public boolean isPublisher() {
        return publisher;
    }

    public String getKind() {
        return kind;
    }

    public boolean isIs_inferred_parent() {
        return is_inferred_parent;
    }

    public String getUnverified_registration_date() {
        return unverified_registration_date;
    }

    public boolean isIs_moderator_or_developer() {
        return is_moderator_or_developer;
    }

    public String getLast_activity() {
        return last_activity;
    }

    public boolean isIs_phantom() {
        return is_phantom;
    }

    public List<String> getProficient_exercises() {
        return proficient_exercises;
    }

    public boolean isCurator() {
        return curator;
    }

    public String getRestricted_domain() {
        return restricted_domain;
    }

    public String getGae_bingo_identity() {
        return gae_bingo_identity;
    }

    public String getHomepage_url() {
        return homepage_url;
    }

    public int getClassroom_user_status() {
        return classroom_user_status;
    }

    public int getNOT_CLASSROOM_USER() {
        return NOT_CLASSROOM_USER;
    }

    public List<String> getFeatured_scratchpads() {
        return featured_scratchpads;
    }

    public int getUserprogresscache_version() {
        return userprogresscache_version;
    }

    public String getKaid() {
        return kaid;
    }

    public Map<String, Integer> getBadge_counts() {
        return badge_counts;
    }

    public KhanUserData(String jsonSource, String jsonString) {
        super(jsonSource, jsonString);
    }

    public KhanUserData(String jsonData) {
        super(jsonData);
    }

    @Override
    public String getDisplayName() {
        return getStudent_summary().getNickname();
    }

    @Override
    public String getUsername() {
        return getStudent_summary().getUsername();
    }

    @Override
    public String getEmail() {
        return getStudent_summary().getEmail();
    }

    @Override
    public Optional<KhanUserData> resolve() {
        KhanUserData element;
        Gson gson = new GsonBuilder().create();
        try {
            element = gson.fromJson(getJsonString(), new TypeToken<KhanUserData>() {
            }.getType());
        } catch (NullPointerException e) {
            element = this;
        }
        return Optional.fromNullable(element);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(KhanUserData.this, new TypeToken<KhanUserData>() {
        }.getType());
    }

    public class StudentSummary {
        @SerializedName("username")
        private String username = "";

        @SerializedName("nickname")
        private String nickname = "";

        @SerializedName("email")
        private String email = "";

        @SerializedName("key")
        private String key = "";

        public String getUsername() {
            return username;
        }

        public String getNickname() {
            return nickname;
        }

        public String getEmail() {
            return email;
        }

        public String getKey() {
            return key;
        }
    }
}

package android.mobile.forged.curious.integrated.khan_academy.utils;

import android.mobile.forged.curious.entities.Topic;

/**
 * Created by visitor15 on 2/18/15.
 */
public class KhanUtils {
    public static final String BASE_URL = "https://www.khanacademy.org";

    public static final String TOPIC_TREE_URL = BASE_URL.concat("/api/v1/topictree");

    public static final String SPECIFIC_TOPIC_URL = BASE_URL.concat("/api/v1/topic");

    public static final String SPECIFIC_VIDEO_TOPIC_URL = BASE_URL.concat("/api/v1/videos");

    public static final String USER_URL = BASE_URL.concat("/api/v1/user");

    public static String getTopicUrl(Topic t) {
        return SPECIFIC_TOPIC_URL.concat("/").concat(t.getId());
    }

    public static String getTopicUrl(String topicId) {
        return SPECIFIC_TOPIC_URL.concat("/").concat(topicId);
    }

    public static String getVideoTopicUrl(String topicId) {
        return SPECIFIC_VIDEO_TOPIC_URL.concat("/").concat(topicId);
    }

    public static String getUserUrl() {
        return USER_URL;
    }
}
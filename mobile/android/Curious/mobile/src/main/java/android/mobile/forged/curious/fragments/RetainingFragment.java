package android.mobile.forged.curious.fragments;

import android.mobile.forged.curious.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Strings;

/**
 * Created by visitor15 on 2/24/15.
 */
public abstract class RetainingFragment extends Fragment {
    private static final String TAG = "RetainFragment";

    public RetainingFragment() {}

    public static RetainingFragment findOrCreateRetainFragment(FragmentManager fm, String tag) {
        tag = (Strings.isNullOrEmpty(tag)) ? TAG : tag;

        RetainingFragment fragment = (RetainingFragment) fm.findFragmentByTag(tag);

        if (fragment == null) {
            fragment = createGenericToolbarFragment();
            fm.beginTransaction().add(fragment, tag).commit();
        }
        return fragment;
    }

    private static RetainingFragment createGenericToolbarFragment() {
        return new RetainingFragment() {

            @Override
            public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
                return inflater.inflate(R.layout.basic_toolbar_layout, container, false);
            }

            @Override
            public void init() {

            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public abstract View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    public abstract void init();
}
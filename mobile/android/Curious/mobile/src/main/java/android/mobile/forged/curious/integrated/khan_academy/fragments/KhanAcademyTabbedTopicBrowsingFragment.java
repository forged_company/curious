package android.mobile.forged.curious.integrated.khan_academy.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.fragments.RootTopicBrowserFragment;
import android.mobile.forged.curious.fragments.YouTubeVideoFragment;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademy;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.mobile.forged.curious.ui.adapters.GenericFragmentPagerAdapter;
import android.mobile.forged.curious.ui.listeners.FragmentCallbackEventListener;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shamanland.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by visitor15 on 2/21/15.
 */
public class KhanAcademyTabbedTopicBrowsingFragment extends RootTopicBrowserFragment {
    private View rootView;

    private KhanTopic topic;

    private LayoutInflater layoutInflater;

    private KhanAcademy khanAcademy;

    private List<KhanTopic> rootTopics;

    private boolean isRootTopics = false;

    private GenericFragmentPagerAdapter fragmentPagerAdapter;

    private ViewPager mViewPager;

    private FragmentCallbackEventListener fragmentCallbackEventListener;

    public KhanAcademyTabbedTopicBrowsingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if(savedInstanceState != null) {
//            extractOutState(savedInstanceState);
//        }
//        else {
            extractArguments();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = inflater;
        rootView = layoutInflater.inflate(R.layout.topic_browsing_fragment_layout, container, false);

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);

        extractOutState(savedInstanceState);
        initKhanService();
        initUIElements();
        initFloatingActionButton();
        return rootView;
    }

    private void initFloatingActionButton() {
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
    }

    private void initUIElements() {
        initFragmentCallbackListener();
        if(fragmentPagerAdapter == null) {
            fragmentPagerAdapter = new GenericFragmentPagerAdapter(getActivity().getSupportFragmentManager());
        }
        mViewPager.setAdapter(fragmentPagerAdapter);
        fragmentPagerAdapter.notifyDataSetChanged();
        if(fragmentPagerAdapter.getCount() == 0) {
            isRootTopics = true;
            khanAcademy.getRootTopics(new DataCallback<List<KhanTopic>>() {
                @Override
                public void receiveResults(List<KhanTopic> results) {
                    rootTopics = results;
                    decorateUIWithResultList(rootTopics);
                }
            });
        }
    }

    private void initFragmentCallbackListener() {
        if(fragmentCallbackEventListener == null) {
            fragmentCallbackEventListener = new FragmentCallbackEventListener() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Toast.makeText(Curious.getContext(), "Got callback event!", Toast.LENGTH_LONG).show();
                    Bundle result = intent.getExtras();
                    if (result != null) {
                        if (result.containsKey("topic")) {
                            KhanTopic t = (KhanTopic) result.getSerializable("topic");

                            if(t.getKind().equalsIgnoreCase("topic")) {
                                getTopicById(t);
                            }
                            else if(t.getKind().equalsIgnoreCase("video")) {
                                getVideoTopicById(t);
                            }
                            else {
                                Toast.makeText(Curious.getContext(), "UNKNOWN VIDEO TYPE!", Toast.LENGTH_LONG).show();
                            }
//                            khanAcademy.getTopicByID(t.getId(), new DataCallback<KhanTopic>() {
//                                @Override
//                                public void receiveResults(KhanTopic results) {
//                                    Bundle args = new Bundle();
//                                    Fragment frag = new RetainingTopicBrowsingFragment();
//                                    args.putSerializable("topic", results);
//                                    frag.setArguments(args);
//                                    if(mViewPager.getCurrentItem() < fragmentPagerAdapter.getCount() - 1) {
//                                        purgeAndInsertFrag(frag);
//                                    } else {
//                                        fragmentPagerAdapter.addData(frag);
//                                        mViewPager.setCurrentItem(fragmentPagerAdapter.getCount() - 1);
//                                    }
//                                }
//                            });
                        }
                    }
                }
            };
        }
    }

    private void getTopicById(Topic t) {
        khanAcademy.getTopicByID(t.getId(), new DataCallback<KhanTopic>() {
            @Override
            public void receiveResults(KhanTopic results) {
                Bundle args = new Bundle();
                Fragment frag = new InformativeKhanAcademyTopicFragment();
                args.putSerializable("topic", results);
                frag.setArguments(args);
                if(mViewPager.getCurrentItem() < fragmentPagerAdapter.getCount() - 1) {
                    purgeAndInsertFrag(frag);
                } else {
                    fragmentPagerAdapter.addData(frag);
                    mViewPager.setCurrentItem(fragmentPagerAdapter.getCount() - 1);
                }

                rootView.findViewById(R.id.pager_title_strip).setVisibility(View.VISIBLE);
            }
        });
    }

    private void getVideoTopicById(KhanTopic t) {
        khanAcademy.getVideoDataForTopic(t, new DataCallback<KhanTopic>() {
            @Override
            public void receiveResults(KhanTopic results) {
                Bundle args = new Bundle();
                Fragment videoFragment = new YouTubeVideoFragment();
                args.putSerializable("topic", results);
                videoFragment.setArguments(args);

                switchFragment(videoFragment);

//                if (mViewPager.getCurrentItem() < fragmentPagerAdapter.getCount() - 1) {
//                    purgeAndInsertFrag(videoFragment);
//                } else {
//                    fragmentPagerAdapter.addData(videoFragment);
//                    mViewPager.setCurrentItem(fragmentPagerAdapter.getCount() - 1);
//                }
//
//                rootView.findViewById(R.id.pager_title_strip).setVisibility(View.GONE);
            }
        });
    }

    private void switchFragment(Fragment newFrag) {
        final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, newFrag)
                .addToBackStack(null)
                .commit();
    }

    private void purgeAndInsertFrag(Fragment frag) {
        int currentIndex = mViewPager.getCurrentItem();
        Fragment rootFrag = fragmentPagerAdapter.getItem(0);
        List<Fragment> retainedFrags = new ArrayList<Fragment>();
        retainedFrags.add(rootFrag);
        for(int i = 1; i <= currentIndex; i++) {
            retainedFrags.add(fragmentPagerAdapter.getItem(i));
        }

        fragmentPagerAdapter = new GenericFragmentPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager.invalidate();
        mViewPager.setAdapter(new GenericFragmentPagerAdapter(getActivity().getSupportFragmentManager()));
        fragmentPagerAdapter = (GenericFragmentPagerAdapter) mViewPager.getAdapter();
        fragmentPagerAdapter.addAll(retainedFrags);
        fragmentPagerAdapter.addData(frag);
        mViewPager.setCurrentItem(fragmentPagerAdapter.getCount() - 1);
    }

    private void extractOutState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            Set<String> keys = savedInstanceState.keySet();
            fragmentPagerAdapter = new GenericFragmentPagerAdapter(getActivity().getSupportFragmentManager());
            for(String k : keys) {
                if(k.contains("topic")) {
                    KhanTopic topic = (KhanTopic) savedInstanceState.get(k);
                    Bundle args = new Bundle();
                    args.putSerializable("topic", topic);
                    Fragment frag = new InformativeKhanAcademyTopicFragment();
                    frag.setArguments(args);
                    fragmentPagerAdapter.addData(frag);
                }
            }
        }


//        if(savedInstanceState != null) {
//            if(savedInstanceState.containsKey("topic")) {
//                topic = (KhanTopic) savedInstanceState.getSerializable("topic");
//            }
//            if(savedInstanceState.containsKey("is_root")) {
//                isRootTopics = savedInstanceState.getBoolean("is_root");
//            }
//        }
    }

    private void extractArguments() {
        Bundle b = getArguments();
        if(b != null) {
            if(b.containsKey("topic")) {
                topic = (KhanTopic) getArguments().getSerializable("topic");
            }
        }
    }

//    private void createResultList() {
//        KhanTopicFactory khanTopicFactory = new KhanTopicFactory();
//        if(topic != null) {
//            List<? extends TopicData> children = topic.getChildren();
//            rootTopics = new ArrayList<KhanTopic>();
//            for(TopicData topicData : children) {
//                JsonData jsonData = (JsonData) topicData;
//                KhanTopic khanTopic = khanTopicFactory.createTopicFromParentTopic(jsonData, topic);
//                rootTopics.add(khanTopic);
//            }
//        }
//        if(isRootTopics) {
//            decorateUIWithResultList(rootTopics);
//        }
//        else {
//            decorateUIWithResultList(rootTopics);
//        }
//    }

    private void decorateUIWithResultList(List<KhanTopic> results) {
        Fragment frag = new InformativeKhanAcademyTopicFragment();

        Bundle args = new Bundle();

        if(topic == null) {
            if(rootTopics != null) {
                frag = InformativeKhanAcademyTopicFragment.createInstance(rootTopics);
            }
        }
        else {
            frag = new InformativeKhanAcademyTopicFragment();
            args.putSerializable("topic", topic);
            frag.setArguments(args);
        }
        fragmentPagerAdapter.addData(frag);
        mViewPager.setCurrentItem(fragmentPagerAdapter.getCount() - 1);
    }

    private void initKhanService() {
        if(khanAcademy == null) {
            khanAcademy = new KhanAcademy(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
                    if(topic == null && rootTopics == null) {
                        initUIElements();
                    }
                }

                @Override
                public void handleServiceMessage(Message msg) {
                    Log.d(getClass().getName(), "GOT SERVICE MESSAGE!");
                }
            });
        }
    }

    private void handleConfigChange(Configuration newConfig) {

    }

    private void handleSaveInstanceState(Bundle outState) {
        if(topic != null) {
            outState.putSerializable("topic", topic);
        }

        outState.putInt("current_index", mViewPager.getCurrentItem());

        // There's probably a better way to do this.
        int frags = fragmentPagerAdapter.getCount();
        for(int i = 0; i < frags; i++) {
            outState.putSerializable("topic_" + i, ((InformativeKhanAcademyTopicFragment) fragmentPagerAdapter.getItem(i)).getTopic());
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentCallbackEventListener.register();
        fragmentPagerAdapter.notifyDataSetChanged();
        mViewPager.invalidate();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        handleSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        handleConfigChange(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
        fragmentCallbackEventListener.unregister();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

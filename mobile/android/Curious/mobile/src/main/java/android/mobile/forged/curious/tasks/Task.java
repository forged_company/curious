package android.mobile.forged.curious.tasks;

import java.io.Serializable;

/**
 * Created by visitor15 on 2/17/15.
 */
public interface Task extends Serializable {
    public void execute();
}

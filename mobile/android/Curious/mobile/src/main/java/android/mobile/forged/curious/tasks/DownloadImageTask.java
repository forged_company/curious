package android.mobile.forged.curious.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.common.base.Strings;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by visitor15 on 3/27/15.
 */
public class DownloadImageTask implements Task {

    private String imageUrl;

    public DownloadImageTask(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public void execute() {
        if(!Strings.isNullOrEmpty(imageUrl)) {
            try {
                downloadImageInternal();
            } catch (IOException e) {
                Log.e(getClass().getSimpleName(), "ERROR downloading image: " + e.getMessage());
            }
        }
    }

    private void downloadImageInternal() throws IOException {
        Bitmap image = null;
            InputStream in = new URL(imageUrl).openStream();
            image = BitmapFactory.decodeStream(in);
    }
}

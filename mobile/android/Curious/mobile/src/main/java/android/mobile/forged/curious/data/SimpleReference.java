package android.mobile.forged.curious.data;

/**
 * Created by visitor15 on 2/24/15.
 */
public interface SimpleReference<T> {
    public T resolve();
}

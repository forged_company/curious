package android.mobile.forged.curious.data;

import android.mobile.forged.curious.entities.Topic;

import java.util.List;

/**
 * Created by visitor15 on 2/18/15.
 */
public interface TopicDataBuilder {

//    public JsonData<? extends TopicData> build(final String jsonSource, final String jsonString);

    public JsonData<List<? extends TopicData>> buildForList(final String jsonSource, final String jsonString);

    public JsonData build(final String jsonSource, final String jsonString);

    public JsonData buildFromJsonString(String jsonData);
}

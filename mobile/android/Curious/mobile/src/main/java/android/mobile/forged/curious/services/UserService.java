package android.mobile.forged.curious.services;

import android.mobile.forged.curious.user.User;
import android.os.Message;

/**
 * Created by visitor15 on 3/2/15.
 */
public class UserService extends LocalService implements SimpleUserService {

    private User currentUser;

    @Override
    protected void handleWorkerMessage(Message msg) {
        handleWorkerMessageInternal(Message.obtain(msg));
    }

    private void handleWorkerMessageInternal(Message msg) {
        switch(msg.what) {
            case Services.EXECUTE_TASK: {
                doTask(Message.obtain(msg));
                break;
            }
            default: {
                break;
            }
        }
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    public static final class Services {
        public static final int EXECUTE_TASK = 0;
    }
}

package android.mobile.forged.curious.integrated.khan_academy.fragments;

import android.mobile.forged.curious.R;
import android.mobile.forged.curious.fragments.BaseFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by visitor15 on 3/4/15.
 */
public class KhanAcademyProfileFragment extends BaseFragment {

    private View rootView;

    private LayoutInflater layoutInflater;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.layoutInflater = inflater;
        rootView = layoutInflater.inflate(R.layout.profile_fragment, container, false);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void initProfileFragment() {

    }
}

package android.mobile.forged.curious.events;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.mobile.forged.curious.ui.adapters.CuriousBroadcastReceiver;
import android.os.Bundle;

/**
 * Created by visitor15 on 2/24/15.
 */
public class FragmentCallbackEvent implements SimpleEvent {
    public static final String ACTION_CALLBACK= "forged_co.curious.action.ACTION_CALLBACK";
    public static final String ACTION_KEY = "forged_co.curious.action.ACTION_KEY";
    public static final String DATA = "forged_co.curious.DATA";
    public static final String FRAGMENT = "forged_co.curious.FRAGMENT";

    public static enum FRAGMENTS {
        PATIENT_FRAGMENT,
        REGISTRATION_FRAGMENT,
        LOGIN_FRAGMENT,
        UNLOCK_DOC_FRAGMENT,
        SEND_DOC_FRAGMENT
    };

    public static enum ACTIONS {
        REPLACE_MAIN_CONTENT,
        SHOW_FRAGMENT_IN_PAGER
    };

    public static IntentFilter createFilter() {
        return new IntentFilter(FragmentCallbackEvent.ACTION_CALLBACK);
    }

    public static void broadcast(final Context context, final Bundle b) {
        final Intent intent = new Intent(ACTION_CALLBACK);
        intent.putExtras(b);
        CuriousBroadcastReceiver.sendLocalBroadcast(context, intent);
    }

    private FragmentCallbackEvent() {
        throw new UnsupportedOperationException();
    }
}
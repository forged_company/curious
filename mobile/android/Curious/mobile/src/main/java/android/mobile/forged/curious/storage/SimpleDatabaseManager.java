package android.mobile.forged.curious.storage;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.user.User;

import com.google.common.base.Optional;

import java.util.Map;

/**
 * Created by visitor15 on 2/17/15.
 */
public abstract class SimpleDatabaseManager extends SQLiteOpenHelper {
    public static final String DB_NAME = "curiousDB";
    public static final int DB_VERSION = 1;

    /**
     * Create a helper object to create, open, and/or manage a database.
     * This method always returns very quickly.  The database is not actually
     * created or opened until one of {@link #getWritableDatabase} or
     * {@link #getReadableDatabase} is called.
     *
     * @param context to use to open or create the database
     * @param name    of the database file, or null for an in-memory database
     * @param factory to use for creating cursor objects, or null for the default
     * @param version number of the database (starting at 1); if the database is older,
     *                {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                newer, {@link #onDowngrade} will be used to downgrade the database
     */
    public SimpleDatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Create a helper object to create, open, and/or manage a database.
     * The database is not actually created or opened until one of
     * {@link #getWritableDatabase} or {@link #getReadableDatabase} is called.
     * <p/>
     * <p>Accepts input param: a concrete instance of {@link android.database.DatabaseErrorHandler} to be
     * used to handle corruption when sqlite reports database corruption.</p>
     *
     * @param context      to use to open or create the database
     * @param name         of the database file, or null for an in-memory database
     * @param factory      to use for creating cursor objects, or null for the default
     * @param version      number of the database (starting at 1); if the database is older,
     *                     {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                     newer, {@link #onDowngrade} will be used to downgrade the database
     * @param errorHandler the {@link android.database.DatabaseErrorHandler} to be used when sqlite reports database
     */
    public SimpleDatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    protected abstract void createDatabase(SQLiteDatabase db, int version);

    protected abstract void upgradeDatabase(SQLiteDatabase db, int newVersion);

    public abstract long persistTopic(final Topic topic);

    public abstract long persistUser(final User user);

    public abstract Topic findTopicByTopicID(final String topicId);

    public abstract long persistAuthentication(User currentUser, Map<String, String> authResults);

    public abstract String getRefreshTokenForUser(User currentUser);

    public abstract String getOauthVerifierForUser(User currentUser);

    public abstract String getSignatureSigningKey(User currentUser);

    public abstract String getAccessTokenForUser(User currentUser);

    public abstract Optional<? extends User> getUser(String userName);

    public abstract String getAccessTokenSignatureSigningKey(User currentUser);
}

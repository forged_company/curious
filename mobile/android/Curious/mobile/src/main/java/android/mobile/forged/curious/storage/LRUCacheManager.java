package android.mobile.forged.curious.storage;

import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicData;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.support.v4.util.LruCache;

import com.google.common.base.Optional;

/**
 * Created by visitor15 on 2/24/15.
 */
public class LRUCacheManager extends LruCache<String, TopicData> {

    // Get max available VM memory, exceeding this amount will throw an
    // OutOfMemory exception. Stored in kilobytes as LruCache takes an
    // int in its constructor.
    final static int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

    // Use 1/8th of the available memory for this memory cache.
    final static int cacheSize = maxMemory / 8;

    public LRUCacheManager() {
        super(cacheSize);
    }

    public synchronized Optional getCachedTopic(final String topicId) {
        KhanTopic topic = null;
        Optional<TopicData> cachedData = Optional.fromNullable(get(topicId));
        if(cachedData.isPresent()) {
            return Optional.of(new KhanTopic((KhanTopicData) cachedData.get()));
        }
        return Optional.absent();
    }

    public synchronized Optional getCachedTopicIncludeDB(final String topicId, final SimpleDatabaseManager dbManager) {
        KhanTopic topic = null;
        Optional<TopicData> cachedData = Optional.fromNullable(get(topicId));
        if(cachedData.isPresent()) {
            return Optional.of(new KhanTopic((KhanTopicData) cachedData.get()));
        }
        else {
            return Optional.fromNullable(dbManager.findTopicByTopicID(topicId));
        }
    }

    public synchronized void cacheTopic(final Topic topic) {
        put(topic.getId(), topic.getTopicData());
    }

    public synchronized long cacheAndPersistTopic(final Topic topic, final SimpleDatabaseManager dbManager) {
        put(topic.getId(), topic.getTopicData());
        return dbManager.persistTopic(topic);
    }
}

package android.mobile.forged.curious.integrated.khan_academy.fragments;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.PaintDrawable;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.events.FragmentCallbackEvent;
import android.mobile.forged.curious.fragments.BaseFragment;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.ui.listeners.FragmentCallbackEventListener;
import android.mobile.forged.curious.ui.listeners.UpdateUIEventListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visitor15 on 2/28/15.
 */
public class InformativeKhanAcademyTopicFragment extends BaseFragment {
    private View rootView;

    private KhanTopic topic;

    private List<KhanTopic> subTopics;

    private LayoutInflater layoutInflater;

    private LinearLayout topicList;

    private KhanTopicFactory khanTopicFactory;

    private List<KhanTopic> rootTopics;

    private UpdateUIEventListener updateUIEventListener;

    public InformativeKhanAcademyTopicFragment() {}

    private InformativeKhanAcademyTopicFragment(List<KhanTopic> topics) {
        this.rootTopics = topics;
    }

    public static InformativeKhanAcademyTopicFragment createInstance(List<KhanTopic> topics) {
        return new InformativeKhanAcademyTopicFragment(topics);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractArgs();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = inflater;
        rootView = layoutInflater.inflate(R.layout.informative_khan_academy_fragment_layout, container, false);

        topicList = (LinearLayout) rootView.findViewById(R.id.topic_container);

        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey("topic")) {
                topic = (KhanTopic) savedInstanceState.getSerializable("topic");
            }
        }

        init();
        initUpdateUIEventListener();

        return rootView;
    }

    public void init() {
        extractArgs();

        khanTopicFactory = new KhanTopicFactory();
        if(topic != null) {

            List<? extends TopicData> children = topic.getChildren();
            subTopics = new ArrayList<KhanTopic>();
            for(TopicData topicData : children) {
                JsonData jsonData = (JsonData) topicData;
                KhanTopic khanTopic = khanTopicFactory.createTopicFromParentTopic(jsonData, topic);
                subTopics.add(khanTopic);
            }
            decorateUIWithResultList(subTopics);
        }
    }

    private void initUpdateUIEventListener() {
        if(updateUIEventListener == null) {
            updateUIEventListener = new UpdateUIEventListener() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Toast.makeText(Curious.getContext(), "Got update UI event!", Toast.LENGTH_LONG).show();
//                    animateBackground();
                }
            };
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden) {
//            getActivity().findViewById(R.id.pager_title_strip).setBackgroundColor(topic.getTopicData().getPrimaryColor());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            if(topic != null) {
                animateBackground();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUIEventListener.register();
        if(khanTopicFactory == null) {
            init();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        updateUIEventListener.unregister();
    }

    private void extractArgs() {
        Bundle args = getArguments();
        if(args != null) {
            if(args.containsKey("topic")) {
                topic = (KhanTopic) args.getSerializable("topic");
            }
        }
        else if(rootTopics != null) {
            decorateUIWithResultList(rootTopics);
        }
    }

    private void animateBackground() {
        View backgroundView = getActivity().findViewById(R.id.pager_container);
        ColorDrawable paintDrawable = ((ColorDrawable) backgroundView.getBackground());
        int currentBackgroundColor = paintDrawable.getColor();
        ObjectAnimator colorFade = ObjectAnimator.ofObject(backgroundView, "backgroundColor", new ArgbEvaluator(), currentBackgroundColor, topic.getTopicData().getPrimaryColor());
        colorFade.setDuration(500);
        colorFade.start();
    }

    private void decorateUIWithResultList(List<KhanTopic> results) {
        ((TextView) rootView.findViewById(R.id.header_description)).setText(topic.getTopicData().getDescription());
//        getActivity().findViewById(R.id.pager_container).setBackgroundColor(topic.getTopicData().getPrimaryColor());
//        rootView.findViewById(R.id.header).setBackgroundColor(topic.getTopicData().getPrimaryColor());
        topicList.removeAllViews();
        for (KhanTopic topic : results) {
            View v;
            if(topic.isVideoTopic()) {
                v = decorateViewAsVideoTopic(topic);
            }
            else {
                v = decorateDefaultTopicView(topic);
            }
            topicList.addView(v);
        }
    }

    private View decorateDefaultTopicView(Topic topic) {
        View topicView = layoutInflater.inflate(R.layout.topic_item_simple_layout, topicList, false);

        ((TextView) topicView.findViewById(R.id.textView_title)).setText(topic.getTopicData().getDisplayName());
//        ((TextView) topicView.findViewById(R.id.textView_description)).setText(topic.getTopicData().getDescription());
        ((TextView) topicView.findViewById(R.id.textView_icon_letter)).setText(topic.getTopicData().getDisplayName().substring(0, 1).toUpperCase());
        ((GradientDrawable) topicView.findViewById(R.id.circle_shape).getBackground()).setColor(topic.getTopicData().getPrimaryColor());

        topicView.setTag(topic);
        topicView.setClickable(true);
        setLinearLayoutClickListener(topicView);

        return topicView;
    }

    private View decorateViewAsVideoTopic(Topic topic) {
        View topicView = layoutInflater.inflate(R.layout.topic_video_item_layout, topicList, false);

        ((TextView) topicView.findViewById(R.id.textView_title)).setText(topic.getTopicData().getDisplayName());
        ((TextView) topicView.findViewById(R.id.textView_description)).setText(topic.getTopicData().getDescription());
        ((GradientDrawable) topicView.findViewById(R.id.imageView_video_icon).getBackground()).setColor(topic.getTopicData().getPrimaryVideoColor());
//            ((TextView) topicView.findViewById(R.id.textView_topicCount)).setText(topic.getChildren().size() + " topics");
        topicView.setTag(topic);
        topicView.setClickable(true);
        setLinearLayoutClickListener(topicView);

        return topicView;
    }

    private void setLinearLayoutClickListener(View v) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Topic t = (Topic) v.getTag();
                Log.d(getClass().getName(), "Clicked topic: " + t.getTopicData().getDisplayName());
                handleTopicClick(t);
            }
        });
    }

    private void handleTopicClick(Topic clickedTopic) {
        broadcastClick(clickedTopic);
    }

    private void broadcastClick(Topic clickedTopic) {
        Bundle b = new Bundle();
        b.putSerializable("topic", clickedTopic);
        FragmentCallbackEvent.broadcast(Curious.getContext(), b);
    }

    private void handleSaveInstanceState(Bundle b) {
        b.putSerializable("topic", topic);
    }

    public Topic getTopic() {
        return topic;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        handleSaveInstanceState(outState);
    }

    @Override
    public String toString() {
        if(topic != null) {
            if(topic.getTopicData() != null) {
                if(topic.getTopicData().getDisplayName() != null) {
                    return topic.getTopicData().getDisplayName();
                }
            }
        }

        return "TEST";
    }
}

package android.mobile.forged.curious.data;

import java.io.Serializable;

/**
 * Created by visitor15 on 2/19/15.
 */
public abstract class DataCallback<T> implements Serializable {

    public abstract void receiveResults(T results);
}
package android.mobile.forged.curious.integrated.khan_academy.entities;

import android.graphics.Color;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicData;

import com.google.common.base.Strings;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by visitor15 on 2/18/15.
 */
public class KhanTopic implements Topic {

    private KhanTopicData _topicData;

    public KhanTopic(final KhanTopicData data) {
        this._topicData = data;
    }

    @Override
    public TopicData getTopicData() {
        return _topicData;
    }

    @Override
    public String getId() {

        if(getKind().equalsIgnoreCase("video")) {
            return _topicData.id;
        }
        else {
            return (Strings.isNullOrEmpty(_topicData.node_slug)) ? ((!Strings.isNullOrEmpty(_topicData.domain_slug)) ? _topicData.domain_slug : _topicData.id) : _topicData.node_slug;
        }
    }

    @Override
    public String getYouTubeId() {
        return !(Strings.isNullOrEmpty(_topicData.translated_youtube_id)) ? _topicData.translated_youtube_id : _topicData.youtube_id;
    }

    @Override
    public List<? extends TopicData> getChildren() {
        return _topicData.children;
    }

    @Override
    public String getUrl() {
        return (!Strings.isNullOrEmpty(_topicData.url)) ? _topicData.url : "";
    }

    @Override
    public String getKind() {
        return _topicData.kind;
    }

    @Override
    public String toString() {
        return getTopicData().toString();
    }

    public boolean isVideoTopic() {
        return getKind().equalsIgnoreCase("video");
    }

    public static class TopicIDs {

        public static final String NEW_AND_NOTEWORY = "new-and-noteworthy";

        public static final String MATH = "math";

        public static final String SCIENCE = "science";

        public static final String ARTS_AND_HUMANITIES = "humanities";

        public static final String COLLEGE_ADMISSION = "college-admissions";

        public static final String COMPUTING = "computing";

        public static final String ECONOMICS_AND_FINANCE = "economics-finance-domain";

        public static final String PARTNER_CONTENT = "partner-content";

        public static final String TALKS_AND_INTERVIEWS = "talks-and-interviews";

        public static final String TEST_PREP = "test-prep";

        public TopicIDs() {}
    }
}

package android.mobile.forged.curious.ui.listeners;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.events.FragmentCallbackEvent;
import android.mobile.forged.curious.ui.adapters.CuriousBroadcastReceiver;

/**
 * Created by visitor15 on 2/24/15.
 */
public abstract class FragmentCallbackEventListener extends CuriousBroadcastReceiver {
    public void register() {
        final IntentFilter filter = FragmentCallbackEvent.createFilter();
        registerLocalReceiver(Curious.getContext(), this, filter);
    }

    public void unregister() {
        unregisterLocalReciever(Curious.getContext(), this);
    }

    @Override
    public abstract void onReceive(Context context, Intent intent);
}
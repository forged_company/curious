package android.mobile.forged.curious.integrated.khan_academy.user;

import android.mobile.forged.curious.data.UserData;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanUserData;
import android.mobile.forged.curious.user.LocalUser;

/**
 * Created by visitor15 on 3/3/15.
 */
public class KhanAcademyUser extends LocalUser {

    private KhanUserData khanUserData;

    public KhanAcademyUser() {

    }

    public KhanAcademyUser(KhanUserData userData) {
        this.khanUserData = userData;
    }

    @Override
    public String getDisplayName() {
        return khanUserData.getDisplayName();
    }

    @Override
    public String getUsername() {
        return khanUserData.getUsername();
    }

    @Override
    public String getEmail() {
        return khanUserData.getEmail();
    }

    @Override
    public UserData getUserData() { return khanUserData; }
}

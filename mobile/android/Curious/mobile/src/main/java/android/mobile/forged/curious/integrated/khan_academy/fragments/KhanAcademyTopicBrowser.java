package android.mobile.forged.curious.integrated.khan_academy.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.events.UpdateUIEvent;
import android.mobile.forged.curious.fragments.BaseFragment;
import android.mobile.forged.curious.fragments.YouTubeVideoFragment;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademy;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.mobile.forged.curious.ui.adapters.GenericFragmentPagerAdapter;
import android.mobile.forged.curious.ui.listeners.FragmentCallbackEventListener;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;
import java.util.Set;

/**
 * Created by visitor15 on 2/28/15.
 */
public class KhanAcademyTopicBrowser extends BaseFragment {
    private View rootView;

    private KhanTopic topic;

    private LayoutInflater layoutInflater;

    private KhanAcademy khanAcademy;

    private List<KhanTopic> rootTopics;

    private boolean isRootTopics = false;

    private GenericFragmentPagerAdapter fragmentPagerAdapter;

    private ViewPager mViewPager;

    private FragmentCallbackEventListener fragmentCallbackEventListener;

    private int currentViewIndex = 0;

    public KhanAcademyTopicBrowser() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractArguments();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = inflater;
        rootView = layoutInflater.inflate(R.layout.topic_browsing_fragment_layout, container, false);

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);

        extractOutState(savedInstanceState);
        initKhanService();
        initUIElements();
//        initFloatingActionButton();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentCallbackEventListener.register();
        fragmentPagerAdapter.notifyDataSetChanged();
        currentViewIndex = mViewPager.getCurrentItem();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        handleSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        handleConfigChange(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
        fragmentCallbackEventListener.unregister();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void extractArguments() {
        Bundle b = getArguments();
        if(b != null) {
            if(b.containsKey("topic")) {
                topic = (KhanTopic) getArguments().getSerializable("topic");
            }
        }
    }

    private void extractOutState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            Set<String> keys = savedInstanceState.keySet();
        }
    }

    private void handleConfigChange(Configuration newConfig) {

    }

    private void handleSaveInstanceState(Bundle outState) {
        if (topic != null) {
            outState.putSerializable("topic", topic);
        }

        outState.putInt("current_index", mViewPager.getCurrentItem());
    }

    private void initFragmentCallbackListener() {
        if(fragmentCallbackEventListener == null) {
            fragmentCallbackEventListener = new FragmentCallbackEventListener() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    Toast.makeText(Curious.getContext(), "Got callback event!", Toast.LENGTH_LONG).show();
                    Bundle result = intent.getExtras();
                    if (result != null) {
                        if (result.containsKey("topic")) {
                            KhanTopic t = (KhanTopic) result.getSerializable("topic");
                            if(t.getKind().equalsIgnoreCase("topic")) {
                                getTopicById(t);
                            }
                            else if(t.getKind().equalsIgnoreCase("video")) {
                                getVideoTopicById(t);
                            }
                            else {
                                Toast.makeText(Curious.getContext(), "UNKNOWN VIDEO TYPE!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            };
        }
    }

    private void getTopicById(Topic t) {
        khanAcademy.getTopicByID(t.getId(), new DataCallback<KhanTopic>() {
            @Override
            public void receiveResults(KhanTopic results) {
                Bundle args = new Bundle();
                args.putSerializable("topic", results);

                Fragment topicBrowserFrag = new KhanAcademyTopicBrowser();
                topicBrowserFrag.setArguments(args);
                switchFragment(topicBrowserFrag);

//                Fragment frag = new RetainingTopicBrowsingFragment();
//
//                frag.setArguments(args);
//                fragmentPagerAdapter.addData(frag);
//                mViewPager.setCurrentItem(fragmentPagerAdapter.getCount() - 1, true);
//
//                rootView.findViewById(R.id.pager_title_strip).setVisibility(View.VISIBLE);
            }
        });
    }

    private void getVideoTopicById(KhanTopic t) {
        khanAcademy.getVideoDataForTopic(t, new DataCallback<KhanTopic>() {
            @Override
            public void receiveResults(KhanTopic results) {
                Bundle args = new Bundle();
                Fragment videoFragment = new YouTubeVideoFragment();
                args.putSerializable("topic", results);
                videoFragment.setArguments(args);
                switchFragment(videoFragment);
            }
        });
    }

    private void switchFragment(Fragment newFrag) {
        final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, newFrag)
                .addToBackStack(null)
                .commit();
    }

    private void decorateUIWithResultList(List<KhanTopic> results) {
        for(Topic t : results) {
            Fragment frag = new InformativeKhanAcademyTopicFragment();

            Bundle b = new Bundle();
            b.putSerializable("topic", t);
            frag.setArguments(b);
            fragmentPagerAdapter.addData(frag);
        }
    }

    private void addInfoTopicFragment(Topic t) {
        Fragment frag = new InformativeKhanAcademyTopicFragment();

        Bundle b = new Bundle();
        b.putSerializable("topic", t);
        frag.setArguments(b);
        fragmentPagerAdapter.addData(frag);
    }

    private void initUIElements() {
        initFragmentCallbackListener();
        if(fragmentPagerAdapter == null) {
            fragmentPagerAdapter = new GenericFragmentPagerAdapter(getChildFragmentManager());
        }
        mViewPager.setAdapter(fragmentPagerAdapter);
        if(fragmentPagerAdapter.getCount() == 0) {
            if(topic != null) {
                addInfoTopicFragment(topic);
            }
            else {
                isRootTopics = true;
                khanAcademy.getRootTopics(new DataCallback<List<KhanTopic>>() {
                    @Override
                    public void receiveResults(List<KhanTopic> results) {
                        rootTopics = results;
                        decorateUIWithResultList(rootTopics);
                        mViewPager.setCurrentItem(currentViewIndex);
                    }
                });
            }
        }
    }

    private void initKhanService() {
        if(khanAcademy == null) {
            khanAcademy = new KhanAcademy(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
                    if(topic == null && rootTopics == null) {
                        initUIElements();
                    }
                }

                @Override
                public void handleServiceMessage(Message msg) {
                    Log.d(getClass().getName(), "GOT SERVICE MESSAGE!");
                }
            });
        }
    }
}

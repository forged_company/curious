package android.mobile.forged.curious.data;

import android.mobile.forged.curious.entities.Topic;

/**
 * Created by visitor15 on 2/18/15.
 */
public interface TopicFactory<T extends Topic> {

    public T createTopic(JsonData data);
}

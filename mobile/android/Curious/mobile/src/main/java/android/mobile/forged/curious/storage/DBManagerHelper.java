package android.mobile.forged.curious.storage;

import android.provider.BaseColumns;

/**
 * Created by visitor15 on 3/2/15.
 */
public class DBManagerHelper {

    public static final String DB_NAME = "user_prefsDB";

    public static final String TOPIC_TABLE_NAME = "user_prefs";

    public static final int DB_VERSION = 1;

    public static final String CREATE_DEFAULT_DATABASE = "CREATE TABLE " + DBEntries.TABLE_NAME_TOPIC + " (" +
            DBEntries._ID + " INTEGER PRIMARY KEY," +
            DBEntries.COLUMN_PREFERENCE_ID + " TEXT, " +
            DBEntries.COLUMN_PREFERENCE_VALUE + " TEXT," +
            DBEntries.COLUMN_PREFERENCE_DEF_VALUE + " TEXT," +
            DBEntries.COLUMN_PREFERENCE_USER_SETTING + " TEXT" + ")";

    public static class DBEntries implements BaseColumns {
        public static final String TABLE_NAME_TOPIC = "user_prefs";
        public static final String COLUMN_PREFERENCE_ID = "preference_id";
        public static final String COLUMN_PREFERENCE_VALUE = "preference_value";
        public static final String COLUMN_PREFERENCE_DEF_VALUE = "default_value";
        public static final String COLUMN_PREFERENCE_USER_SETTING = "use_user_setting";
    }
}

package android.mobile.forged.curious.integrated.khan_academy.data;

import android.graphics.Color;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.data.TopicDecorator;
import android.mobile.forged.curious.entities.Topic;

/**
 * Created by visitor15 on 2/19/15.
 */
public class KhanTopicDecorator implements TopicDecorator {

    public KhanTopicDecorator() {

    }

    @Override
    public Topic decorateTopic(Topic topic) {
        return decorateTopicInternal(topic);
    }

    private Topic decorateTopicInternal(Topic topic) {
        // TODO: implement topic-specific look and feel.
        return topic;
    }
}

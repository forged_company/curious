package android.mobile.forged.curious.activities;

import android.content.Intent;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademyClientAuthenticator;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by visitor15 on 3/2/15.
 */
public class AuthenticationCallbackActivityHandler extends ActionBarActivity {


    private KhanAcademyClientAuthenticator khanAcademyClientAuthenticator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(khanAcademyClientAuthenticator == null) {
            khanAcademyClientAuthenticator = new KhanAcademyClientAuthenticator(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
                    persistCredentials(getIntent().getData());
                }

                @Override
                public void handleServiceMessage(Message msg) {

                }
            });
        }

//        Intent i = getIntent();
//
//        if(i != null) {
//            Uri uri = i.getData();
//            Log.d(getClass().getName(), "GOT URI: " + uri.toString());
//            persistCredentials(uri);
//            startActivity(new Intent(getApplicationContext(), MainActivit;y.class));
//        }
    }

    private void persistCredentials(Uri uri) {
        Map<String, String> authResults = null;
//        authResults = handleAccessTokenRequest(uri);
//        Curious.getKhanDBManager().persistAuthentication(Curious.getCurrentUser(), authResults);

        boolean continueAuthProcess = false;
        if(uri.getHost().equalsIgnoreCase("auth-refresh-callback")) {
            authResults = handleRefreshTokenRequest(uri);
            continueAuthProcess = true;
        }
        else if(uri.getHost().equalsIgnoreCase("auth-access-callback")) {
            authResults = handleAccessTokenRequest(uri);
        }

        Curious.getKhanDBManager().persistAuthentication(Curious.getCurrentUser(), authResults);
        if(continueAuthProcess) {
            khanAcademyClientAuthenticator.authenticateUser(new KhanAcademyUser() {
            }, new DataCallback() {
                @Override
                public void receiveResults(Object results) {
                    Log.d(getClass().getName(), "GOT AUTH RESULTS!");
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            });
        }
    }

    private Map<String, String> handleRefreshTokenRequest(Uri uri) {
        StringBuilder strBuilder = new StringBuilder();
        List<String> params = new ArrayList<String>();
        Set<String> paramNames = uri.getQueryParameterNames();
        Map<String, String> authResults = new HashMap<String, String>();
        for(String name : paramNames) {
            params.add(uri.getQueryParameter(name));
            strBuilder.append("GOT PARAM: " + name + " - " + uri.getQueryParameter(name) + "\n");
            Log.d(getClass().getName(), "GOT PARAM: " + name + " - " + uri.getQueryParameter(name));

            // Hacky
            if(name.equalsIgnoreCase("oauth_token")) {
                name = "oauth_refresh_token";
                authResults.put(name, uri.getQueryParameter("oauth_token"));
            }
            else if(name.equalsIgnoreCase("oauth_token_secret")) {
                name = "oauth_refresh_token_secret";
                authResults.put(name, uri.getQueryParameter("oauth_token_secret"));
            }
            else {
                authResults.put(name, uri.getQueryParameter(name));
            }
        }
        Toast.makeText(getApplicationContext(), strBuilder.toString(), Toast.LENGTH_LONG).show();
        return authResults;
    }

    private Map<String, String> handleAccessTokenRequest(Uri uri) {
        StringBuilder strBuilder = new StringBuilder();
        Set<String> paramNames = uri.getQueryParameterNames();
        List<String> params = new ArrayList<String>();
        Map<String, String> authResults = new HashMap<String, String>();
        for(String name : paramNames) {
            params.add(uri.getQueryParameter(name));
            strBuilder.append("GOT PARAM: " + name + " - " + uri.getQueryParameter(name) + "\n");
            Log.d(getClass().getName(), "GOT PARAM: " + name + " - " + uri.getQueryParameter(name));
            authResults.put(name, uri.getQueryParameter(name));
        }
        Toast.makeText(getApplicationContext(), strBuilder.toString(), Toast.LENGTH_LONG).show();
        return authResults;
    }
}

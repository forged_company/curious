package android.mobile.forged.curious.integrated.khan_academy.services;

import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.data.UserData;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicDataBuilder;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.integrated.khan_academy.utils.KhanUtils;
import android.mobile.forged.curious.networking.http.HttpConnector;
import android.mobile.forged.curious.services.LocalService;
import android.mobile.forged.curious.storage.CacheManager;
import android.mobile.forged.curious.user.User;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

import java.util.List;

/**
 * Created by visitor15 on 2/17/15.
 */
public class KhanService extends LocalService {

    private HttpConnector httpConnector;

    private KhanTopicDataBuilder topicDataBuilder;

    private CacheManager cacheManager;

    public KhanService() {
        httpConnector = new HttpConnector();
        topicDataBuilder = new KhanTopicDataBuilder();
        cacheManager = new CacheManager();
    }

    @Override
    protected void handleWorkerMessage(Message msg) {
        msg.getData().putBundle("result_set", handleRequest(msg.what, msg.getData()));
        handleResponse(Message.obtain(msg));
        killCurrentWorkerThread();
    }

    private void handleResponse(Message msg) {
        try {
            msg.replyTo.send(Message.obtain(msg));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private Bundle handleRequest(final int reqCommand, Bundle requestBundle) {
        Bundle b = null;

        switch(reqCommand) {
            case Services.REQUEST_ARTS_AND_HUMANITIES_TOPICS: {
                b = getAllArtsAndHumanitiesTopics();
                break;
            }
            case Services.REQUEST_COMPUTING_TOPICS: {
                b = getAllComputingTopics();
                break;
            }
            case Services.REQUEST_ECONOMICS_AND_FINANCE_TOPICS: {
                b = getAllEconomicsAndFinanceTopics();
                break;
            }
            case Services.REQUEST_MATH_TOPICS: {
                b = getAllMathTopics();
                break;
            }
            case Services.REQUEST_SCIENCE_TOPICS: {
                b = getAllScienceTopics();
                break;
            }
            case Services.REQUEST_TALKS_AND_INTERVIEW_TOPICS: {
                b = getAllTalksAndInterviews();
                break;
            }
            case Services.REQUEST_TEST_PREP_TOPICS: {
                b = getAllTestPrepTopics();
                break;
            }
            case Services.REQUEST_PARTNER_CONTENT_TOPICS: {
                b = getAllPartnerContentTopics();
                break;
            }
            case Services.REQUEST_ROOT_TOPICS: {
                b = getRootTopics();
                break;
            }
            case Services.REQUEST_NEW_AND_NOTEWORTHY_TOPICS: {
                b = getNewAndNoteworthyTopics();
                break;
            }
            case Services.REQUEST_TOPIC_BY_TOPIC_ID: {
                b = getTopicByTopicId(requestBundle.getString("topic_id"));
                break;
            }
            case Services.REQUEST_VIDEO_DATA_BY_TOPIC_ID: {
                b = getVideoDataByTopicId(requestBundle.getString("topic_id"));
                break;
            }
            case Services.REQUEST_USER: {
                b = getUser(requestBundle.getString("user_name"));
                break;
            }
            default: {
                b = createFailedResultSet("", "", "UNKNOWN_REQUEST");
            }
        }

        return b;
    }

    private Bundle getUser(final String userName) {
        Bundle b = new Bundle();
        Optional<KhanAcademyUser> user = (Optional<KhanAcademyUser>) Curious.getKhanDBManager().getUser(userName);
        if(user.isPresent()) {
            b.putSerializable("user", user.get());
        }
        else {
            String url = KhanUtils.getUserUrl();



//            Optional<Bundle> cachedResults = checkAgainstCache(topicId);
//            if(cachedResults.isPresent()) {
//                return cachedResults.get();
//            }

            // TODO we need a builder.
            User newUser = new User() {
                @Override
                public String getDisplayName() {
                    return "";
                }

                @Override
                public String getUsername() {
                    return userName;
                }

                @Override
                public String getEmail() {
                    return "";
                }

                @Override
                public UserData getUserData() {
                    return null;
                }
            };

            String result = Curious.getAuthService().makeAuthenticatedGETRequest(newUser, url);
            b.putString("result_set", result);
        }

        return b;
    }

    private void addToCache(String topicId, JsonData data) {
        Curious.getCacheManager().put(topicId, (TopicData) data);
    }

    private void addToCacheAndPersistToDB(String topicId, JsonData jsonData) {
        KhanTopicFactory topicFactory = new KhanTopicFactory();
        Curious.getCacheManager().put(topicId, (TopicData) jsonData);
        Curious.getKhanDBManager().persistTopic(topicFactory.createTopic(jsonData));
    }

    private Bundle createSuccessfullResultSet(final String topicId, final String jsonSource, final String resultData) {
        Bundle b = new Bundle();

        JsonData resultSet = topicDataBuilder.build(jsonSource, resultData);
        addToCacheAndPersistToDB(topicId, resultSet);

        b.putSerializable("result_set", resultSet);

        return b;
    }

    private Bundle createFailedResultSet(final String jsonSource, final String resultData, final String errorMessage) {
        Bundle b = new Bundle();

        b.putString("request_error", errorMessage);
        b.putString("source", jsonSource);
//        if(!Strings.isNullOrEmpty(resultData)) {
//            b.putString("result_set", resultData);
//        }
//        else {
//            b.putString("result_set", "");
//        }

        return b;
    }

    private Optional<Bundle> checkAgainstCache(String topicId) {
        return checkAgainstCacheAndDatabase(topicId);
//        Optional<KhanTopic> cachedData = Curious.getCacheManager().getCachedTopic(topicId);
//        Bundle cachedResults = null;
//        if(cachedData.isPresent()) {
//            cachedResults = new Bundle();
//            cachedResults.putSerializable("result_set", cachedData.get().getTopicData());
//        }
//        return Optional.fromNullable(cachedResults);
    }

    private Optional<Bundle> checkAgainstCacheAndDatabase(String topicId) {
        Optional<KhanTopic> cachedData = Curious.getCacheManager().getCachedTopicIncludeDB(topicId, Curious.getKhanDBManager());
        Bundle cachedResults = null;
        if(cachedData.isPresent()) {
            cachedResults = new Bundle();
            cachedResults.putSerializable("result_set", cachedData.get().getTopicData());
        }
        return Optional.fromNullable(cachedResults);
    }

    public Bundle getTopicByTopicId(final String topicId) {
        String url = KhanUtils.getTopicUrl(topicId);

        Optional<Bundle> cachedResults = checkAgainstCache(topicId);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(topicId, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getVideoDataByTopicId(final String topicId) {
        String url = KhanUtils.getVideoTopicUrl(topicId);

        Optional<Bundle> cachedResults = checkAgainstCache(topicId);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(topicId, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getNewAndNoteworthyTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.NEW_AND_NOTEWORY);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.NEW_AND_NOTEWORY);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.NEW_AND_NOTEWORY, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllMathTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.MATH);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.MATH);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.MATH, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllScienceTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.SCIENCE);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.SCIENCE);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.SCIENCE, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllEconomicsAndFinanceTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.ECONOMICS_AND_FINANCE);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.ECONOMICS_AND_FINANCE);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.ECONOMICS_AND_FINANCE, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllArtsAndHumanitiesTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.ARTS_AND_HUMANITIES);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.ARTS_AND_HUMANITIES);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.ARTS_AND_HUMANITIES, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllComputingTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.COMPUTING);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.COMPUTING);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.COMPUTING, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllTestPrepTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.TEST_PREP);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.TEST_PREP);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.TEST_PREP, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllPartnerContentTopics() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.PARTNER_CONTENT);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.PARTNER_CONTENT);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.PARTNER_CONTENT, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getAllTalksAndInterviews() {
        String url = KhanUtils.getTopicUrl(KhanTopic.TopicIDs.TALKS_AND_INTERVIEWS);

        Optional<Bundle> cachedResults = checkAgainstCache(KhanTopic.TopicIDs.TALKS_AND_INTERVIEWS);
        if(cachedResults.isPresent()) {
            return cachedResults.get();
        }

        String result = httpConnector.postForResponse(url);

        if(!Strings.isNullOrEmpty(result)) {
            return createSuccessfullResultSet(KhanTopic.TopicIDs.TALKS_AND_INTERVIEWS, url, result);
        }
        else {
            return createFailedResultSet(url, result, "ERROR REQUESTING DATA FOR: " + url);
        }
    }

    public Bundle getRootTopics() {
        Bundle b = new Bundle();

        b.putBundle(KhanTopic.TopicIDs.NEW_AND_NOTEWORY, getNewAndNoteworthyTopics());
        b.putBundle(KhanTopic.TopicIDs.ARTS_AND_HUMANITIES, getAllArtsAndHumanitiesTopics());
        b.putBundle(KhanTopic.TopicIDs.COMPUTING, getAllComputingTopics());
        b.putBundle(KhanTopic.TopicIDs.ECONOMICS_AND_FINANCE, getAllEconomicsAndFinanceTopics());
        b.putBundle(KhanTopic.TopicIDs.MATH, getAllMathTopics());
        b.putBundle(KhanTopic.TopicIDs.PARTNER_CONTENT, getAllPartnerContentTopics());
        b.putBundle(KhanTopic.TopicIDs.SCIENCE, getAllScienceTopics());
        b.putBundle(KhanTopic.TopicIDs.TALKS_AND_INTERVIEWS, getAllTalksAndInterviews());
        b.putBundle(KhanTopic.TopicIDs.TEST_PREP, getAllTestPrepTopics());

        return b;
    }

    public static final class Services {
        public static final int REQUEST_MATH_TOPICS = 0;
        public static final int REQUEST_SCIENCE_TOPICS= 1;
        public static final int REQUEST_ECONOMICS_AND_FINANCE_TOPICS = 2;
        public static final int REQUEST_ARTS_AND_HUMANITIES_TOPICS = 3;
        public static final int REQUEST_COMPUTING_TOPICS = 4;
        public static final int REQUEST_TEST_PREP_TOPICS = 5;
        public static final int REQUEST_PARTNER_CONTENT_TOPICS = 6;
        public static final int REQUEST_TALKS_AND_INTERVIEW_TOPICS = 7;
        public static final int REQUEST_NEW_AND_NOTEWORTHY_TOPICS = 8;
        public static final int REQUEST_ROOT_TOPICS = 9;
        public static final int REQUEST_TOPIC_BY_TOPIC_ID = 10;
        public static final int REQUEST_VIDEO_DATA_BY_TOPIC_ID = 11;
        public static final int REQUEST_USER = 12;
    }
}

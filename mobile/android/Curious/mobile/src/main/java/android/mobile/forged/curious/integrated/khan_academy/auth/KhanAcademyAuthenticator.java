package android.mobile.forged.curious.integrated.khan_academy.auth;

import android.content.Intent;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.services.AuthService;
import android.mobile.forged.curious.user.User;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.common.base.Strings;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by visitor15 on 3/1/15.
 */
public class KhanAcademyAuthenticator extends AuthService {

//    private static String key = "pczKaAE5nbCr6XSG";
//    private static String secret = "WFrZs3SA4BhQgRtR";

    private static String key = "nz477AfFxsDAgfRb";
    private static String secret = "n3eEGDtMFVmxqyKJ";

    private static final String HMAC_SHA1 = "HMAC-SHA1";

    private static final String ENC = "UTF-8";

    @Override
    public Bundle authenticate(User user) {
        return authenticateInternal(user);
    }

    @Override
    public String makeAuthenticatedGETRequest(User user, String urlStr) {
        String token = Curious.getKhanDBManager().getAccessTokenForUser(user);
        String signingKey = Curious.getKhanDBManager().getAccessTokenSignatureSigningKey(Curious.getCurrentUser());
        try {
            if (Strings.isNullOrEmpty(token)) {
                makeRequestForRequestToken();
            }
            HttpClient httpclient = new DefaultHttpClient();
            List<NameValuePair> qparams = new ArrayList<NameValuePair>();
            qparams.add(new BasicNameValuePair("oauth_consumer_key", key));
            qparams.add(new BasicNameValuePair("oauth_nonce", ""
                    + (int) (Math.random() * 100000000)));
            qparams.add(new BasicNameValuePair("oauth_signature_method",
                    "HMAC-SHA1"));
            qparams.add(new BasicNameValuePair("oauth_timestamp", ""
                    + (System.currentTimeMillis() / 1000)));
            qparams.add(new BasicNameValuePair("oauth_token", token));
            qparams.add(new BasicNameValuePair("oauth_version", "1.0"));
            String encodedSecret = URLEncoder.encode(secret);
            String encodedTokenSecret = URLEncoder.encode(signingKey);
            String signature = getSignature(URLEncoder.encode(
                    urlStr, ENC), URLEncoder.encode(URLEncodedUtils.format(qparams, ENC), ENC), encodedSecret + "&" + encodedTokenSecret);
            // add it to params list
            qparams.add(new BasicNameValuePair("oauth_signature", signature));

            URL url = new URL(urlStr);
            URI uri = URIUtils.createURI(url.getProtocol(), url.getAuthority(), -1,
                    url.getPath(),
                    URLEncodedUtils.format(qparams, ENC), null);

            HttpGet httpget = new HttpGet(uri);
            // output the response content.
            HttpResponse response = httpclient.execute(httpget);
            if(response.getStatusLine().getStatusCode() == 401) {
                // Expired token
                handleExpiredToken();
            }

            HttpEntity httpEntity = response.getEntity();
            InputStream instream = httpEntity.getContent();
            int len;
            byte[] tmp = new byte[2048];
            StringBuilder stringBuilder = new StringBuilder();
            while ((len = instream.read(tmp)) != -1) {
                String s = new String(tmp, 0, len, ENC);
                stringBuilder.append(s);
            }
            Log.d("TAG", "RESULT: " + stringBuilder.toString());
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }



    private Bundle authenticateInternal(User user) {
        try {
//            makeRequestForRequestToken();
            makeRequestForAccessToken();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Log.d(getClass().getSimpleName(), "RETURN RESULTS:");
        return handleResponse();
    }

    private Bundle handleResponse() {
        Bundle responseData = new Bundle();

        String accessToken = Curious.getKhanDBManager().getAccessTokenForUser(Curious.getCurrentUser());
        responseData.putString("token", accessToken);

        return responseData;
    }

    private void makeRequestForRequestToken() throws IOException, URISyntaxException, InvalidKeyException, NoSuchAlgorithmException {
        HttpClient httpclient = new DefaultHttpClient();
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        // These params should ordered in key
        qparams.add(new BasicNameValuePair("oauth_callback", "forged-curious://auth-refresh-callback"));
        qparams.add(new BasicNameValuePair("oauth_consumer_key", key));
        qparams.add(new BasicNameValuePair("oauth_nonce", ""
                + (int) (Math.random() * 100000000)));
        qparams.add(new BasicNameValuePair("oauth_signature_method",
                "HMAC-SHA1"));
        qparams.add(new BasicNameValuePair("oauth_timestamp", ""
                + (System.currentTimeMillis() / 1000)));
        qparams.add(new BasicNameValuePair("oauth_version", "1.0"));

        // generate the oauth_signature
        String signature = getSignature(URLEncoder.encode(
                        "https://www.khanacademy.org/api/auth/request_token", ENC),
                URLEncoder.encode(URLEncodedUtils.format(qparams, ENC), ENC), URLEncoder.encode(secret, ENC) + "&");

        // add it to params list
        qparams.add(new BasicNameValuePair("oauth_signature", signature));

        // generate URI which lead to access_token and token_secret.
        URI uri = URIUtils.createURI("https", "www.khanacademy.org", -1,
                "/api/auth/request_token",
                URLEncodedUtils.format(qparams, ENC), null);

        System.out.println("Get Token and Token Secrect from:"
                + uri.toString());

        doIntentRequest(uri.toString());

        killCurrentWorkerThread();

//        URL url = uri.toURL();
//        HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
//        ucon.setInstanceFollowRedirects(false);
//        Map<String, List<String>> headerField = ucon.getHeaderFields();
//        URL secondURL = new URL(ucon.getHeaderField("Location"));
//        URLConnection conn = secondURL.openConnection();
//
//
//        HttpGet httpget = new HttpGet(uri);
//        // output the response content.
//        System.out.println("oken and Token Secrect:");
//
//        HttpResponse response = httpclient.execute(httpget);
//        HttpEntity entity = response.getEntity();
//
//        if (entity != null) {
//            InputStream instream = entity.getContent();
//            int len;
//            byte[] tmp = new byte[2048];
//            StringBuilder stringBuilder = new StringBuilder();
//            while ((len = instream.read(tmp)) != -1) {
//                String s = new String(tmp, 0, len, ENC);
//                Log.d(getClass().getName(), s);
////                System.out.println(s);
//                stringBuilder.append(s);
//            }
//
//            Log.d(getClass().getName(), stringBuilder.toString());
//        }
    }

    private void makeRequestForAccessToken() throws IOException, URISyntaxException, InvalidKeyException, NoSuchAlgorithmException {

        String token = Curious.getKhanDBManager().getRefreshTokenForUser(Curious.getCurrentUser());
        String verifier = Curious.getKhanDBManager().getOauthVerifierForUser(Curious.getCurrentUser());
        String signingKey = Curious.getKhanDBManager().getSignatureSigningKey(Curious.getCurrentUser());

        if(Strings.isNullOrEmpty(token) || Strings.isNullOrEmpty(verifier)) {
            makeRequestForRequestToken();
            return;
        }

        HttpClient httpclient = new DefaultHttpClient();
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        // These params should ordered in keyz
        qparams.add(new BasicNameValuePair("oauth_callback", "forged-curious://auth-access-callback"));
        qparams.add(new BasicNameValuePair("oauth_consumer_key", key));
        qparams.add(new BasicNameValuePair("oauth_nonce", ""
                + (int) (Math.random() * 100000000)));
        qparams.add(new BasicNameValuePair("oauth_signature_method",
                "HMAC-SHA1"));
        qparams.add(new BasicNameValuePair("oauth_timestamp", ""
                + (System.currentTimeMillis() / 1000)));

        qparams.add(new BasicNameValuePair("oauth_token", token));
        qparams.add(new BasicNameValuePair("oauth_verifier", verifier));

        qparams.add(new BasicNameValuePair("oauth_version", "1.0"));
        // generate the oauth_signature

        String encodedSecret = URLEncoder.encode(secret);
        String encodedTokenSecret = URLEncoder.encode(signingKey);

        String signature = getSignature(URLEncoder.encode(
                "https://www.khanacademy.org/api/auth/access_token", ENC), URLEncoder.encode(URLEncodedUtils.format(qparams, ENC), ENC), encodedSecret + "&" + encodedTokenSecret);

        // add it to params list
        qparams.add(new BasicNameValuePair("oauth_signature", signature));

        // generate URI which lead to access_token and token_secret.
        URI uri = URIUtils.createURI("https", "www.khanacademy.org", -1,
                "/api/auth/access_token",
                URLEncodedUtils.format(qparams, ENC), null);

        System.out.println("Get Token and Token Secrect from:"
                + uri.toString());

        URL url = uri.toURL();
        HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
        ucon.setInstanceFollowRedirects(false);

        HttpGet httpget = new HttpGet(uri);
        // output the response content.
        HttpResponse response = httpclient.execute(httpget);

        if(response.getStatusLine().getStatusCode() == 401) {
            // Expired token
            handleExpiredToken();
        }
        else {
            HttpEntity entity = response.getEntity();
            handleAccessTokenFromHttpEntity(entity);
        }
    }

    private void handleAccessTokenFromHttpEntity(final HttpEntity httpEntity) throws IOException {
        if (httpEntity != null) {
            InputStream instream = httpEntity.getContent();
            int len;
            byte[] tmp = new byte[2048];
            StringBuilder stringBuilder = new StringBuilder();
            while ((len = instream.read(tmp)) != -1) {
                String s = new String(tmp, 0, len, ENC);
                stringBuilder.append(s);
            }

            String tokenString = stringBuilder.toString();
            Log.d(getClass().getName(), tokenString);
            // oauth_token=t4824795287650304&oauth_token_secret=LdgL7k9ruYM5VJGL
            String[] elements = tokenString.split("&");
            Map<String, String> authResults = new HashMap<String, String>();
            for(int i = 0; i < elements.length; i++) {
                String str = elements[i];
                String data = "";
                if(str.contains("oauth_token=")) {
                    data = str.substring("oauth_token=".length());
                    authResults.put("oauth_token", data);
                }
                else if(str.contains("oauth_token_secret=")) {
                    data = str.substring("oauth_token_secret=".length());
                    authResults.put("oauth_token_secret", data);
                }
            }

            long result = Curious.getKhanDBManager().persistAuthentication(Curious.getCurrentUser(), authResults);
            Log.d("TAG", "AUTH RESULT: " + result);
        }
    }

    private void handleExpiredToken() {
        try {
            makeRequestForRequestToken();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private static String getSignature(String url, String params, String signingKey)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {

        /* PROTOCOL, URL, PARAMS */
        StringBuilder base = new StringBuilder();
        base.append("GET&");
        base.append(url);
        base.append("&");
        base.append(params);
        System.out.println("String for oauth_signature generation:" + base);

        byte[] keyBytes = (signingKey).getBytes(ENC);

        SecretKey key = new SecretKeySpec(keyBytes, HMAC_SHA1);

        Mac mac = Mac.getInstance(HMAC_SHA1);
        mac.init(key);

        return new String(Base64.encode(mac.doFinal(base.toString().getBytes()), Base64.DEFAULT)).trim();
    }

    private void doIntentRequest(String url) {
        // TODO This needs to change to keep the user inside the application.
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url)).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_FROM_BACKGROUND | Intent.FLAG_ACTIVITY_NEW_TASK);
        Curious.getContext().startActivity(intent);
        // Fire and forget.
        killCurrentWorkerThread();
    }

    private class NonceGenerator {
        private Random ran;

        public Long generateNonce() {
            ran = new Random();
            return new Long(ran.nextLong());
        }
    }
}

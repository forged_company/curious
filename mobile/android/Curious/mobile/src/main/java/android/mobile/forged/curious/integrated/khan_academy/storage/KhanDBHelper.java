package android.mobile.forged.curious.integrated.khan_academy.storage;

import android.provider.BaseColumns;

/**
 * Created by nchampagne on 2/18/15.
 */
public class KhanDBHelper {

    public static final String DB_NAME = "khanDB";

    public static final int DB_VERSION = 1;

    public static final String CREATE_DEFAULT_DATABASE = "CREATE TABLE " + DBEntries.TABLE_NAME_TOPIC + " (" +
            DBEntries._ID + " INTEGER PRIMARY KEY," +
            DBEntries.COLUMN_TOPIC_ID + " TEXT, " +
            DBEntries.COLUMN_DISPLAY_TITLE + " TEXT," +
            DBEntries.COLUMN_DOMAIN_SLUG + " TEXT," +
            DBEntries.COLUMN_PARENT_DOMAIN + " TEXT," +
            DBEntries.COLUMN_TOPIC_TYPE + " TEXT," +
            DBEntries.COLUMN_JSON_DATA + " TEXT " + ")";

    public static final String CREATE_AUTH_DATABASE = "CREATE TABLE " + DBEntries.TABLE_NAME_AUTH + " (" +
            DBEntries._ID + " INTEGER PRIMARY KEY," +
            DBEntries.COLUMN_USERNAME + " TEXT, " +
            DBEntries.COLUMN_USERDATA + " TEXT, " +
            DBEntries.COLUMN_REFRESH_TOKEN + " TEXT, " +
            DBEntries.COLUMN_OAUTH_VERIFIER + " TEXT, " +
            DBEntries.COLUMN_TOKEN + " TEXT, " +
            DBEntries.COLUMN_TOKEN_SECRET + " TEXT, " +
            DBEntries.COLUMN_REFRESH_TOKEN_SECRET + " TEXT " + ")";

    public static final String CREATE_USER_TABLE = "CREATE TABLE " + DBEntries.TABLE_NAME_USERS + " (" +
            DBEntries._ID + " INTEGER PRIMARY KEY," +
            DBEntries.COLUMN_USERNAME + " TEXT, " +
            DBEntries.COLUMN_USERDATA + " TEXT " + ")";

    public static class DBEntries implements BaseColumns {
        public static final String TABLE_NAME_USERS = "khan_users";
        public static final String TABLE_NAME_TOPIC = "khan_topics";
        public static final String COLUMN_TOPIC_ID = "topic_id";
        public static final String COLUMN_DISPLAY_TITLE = "display_title";
        public static final String COLUMN_DOMAIN_SLUG = "domain_slug";
        public static final String COLUMN_PARENT_DOMAIN = "parent_domain";
        public static final String COLUMN_TOPIC_TYPE = "topic_type";
        public static final String COLUMN_JSON_DATA = "json_data";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_USERDATA = "user_data";
        public static final String COLUMN_AUTHENTICATION = "user_auth";
        public static final String COLUMN_REFRESH_TOKEN = "refresh_token";
        public static final String COLUMN_OAUTH_VERIFIER = "oauth_verifier";
        public static final String COLUMN_TOKEN = "token";
        public static final String TABLE_NAME_AUTH = "khan_auth";
        public static final String COLUMN_TOKEN_SECRET = "token_secret";
        public static final String COLUMN_REFRESH_TOKEN_SECRET = "refresh_token_secret";
    }
}

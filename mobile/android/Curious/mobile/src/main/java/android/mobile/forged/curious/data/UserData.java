package android.mobile.forged.curious.data;

/**
 * Created by visitor15 on 3/3/15.
 */
public interface UserData {

    public String getDisplayName();

    public String getUsername();

    public String getEmail();
}

package android.mobile.forged.curious.events;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.mobile.forged.curious.ui.adapters.CuriousBroadcastReceiver;
import android.os.Bundle;

/**
 * Created by visitor15 on 3/2/15.
 */
public class UpdateNavigationDrawerEvent implements SimpleEvent {
    public static final String ACTION_UPDATE_NAV_DRAWER = "forged_co.curious.action.ACTION_UPDATE_NAVIGATIN_DRAWER";
    public static final String ACTION_KEY = "forged_co.curious.action.ACTION_KEY";
    public static final String DATA = "forged_co.curious.DATA";
    public static final String FRAGMENT = "forged_co.curious.FRAGMENT";

    public static enum FRAGMENTS {
        PATIENT_FRAGMENT,
        REGISTRATION_FRAGMENT,
        LOGIN_FRAGMENT,
        UNLOCK_DOC_FRAGMENT,
        SEND_DOC_FRAGMENT
    };

    public static enum ACTIONS {
        UPDATE_UI,
    };

    public static IntentFilter createFilter() {
        return new IntentFilter(FragmentCallbackEvent.ACTION_CALLBACK);
    }

    public static void broadcast(final Context context, final Bundle b) {
        final Intent intent = new Intent(ACTION_UPDATE_NAV_DRAWER);
        intent.putExtras(b);
        CuriousBroadcastReceiver.sendLocalBroadcast(context, intent);
    }

    private UpdateNavigationDrawerEvent() {
        throw new UnsupportedOperationException();
    }
}
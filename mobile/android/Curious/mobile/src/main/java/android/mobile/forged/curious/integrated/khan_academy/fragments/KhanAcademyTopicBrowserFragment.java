package android.mobile.forged.curious.integrated.khan_academy.fragments;

import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.mobile.forged.curious.R;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.fragments.RootTopicBrowserFragment;
import android.mobile.forged.curious.fragments.YouTubeVideoFragment;
import android.mobile.forged.curious.integrated.khan_academy.clients.KhanAcademy;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.shamanland.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visitor15 on 2/21/15.
 */
public class KhanAcademyTopicBrowserFragment extends RootTopicBrowserFragment {
    private View rootView;

    private KhanTopic topic;

    private LayoutInflater layoutInflater;

    private KhanAcademy khanAcademy;

    private LinearLayout topicContainer;

    private List<KhanTopic> rootTopics;

    private boolean isRootTopics = false;

    public KhanAcademyTopicBrowserFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            extractOutState(savedInstanceState);
        }
        else {
            extractArguments();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutInflater = inflater;
        rootView = layoutInflater.inflate(R.layout.root_topic_browser_fragment_layout, container, false);
        topicContainer = (LinearLayout) rootView.findViewById(R.id.topic_container);
        extractOutState(savedInstanceState);
        initKhanService();
        initUIElements();
        initFloatingActionButton();
        return rootView;
    }

    private void initFloatingActionButton() {
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
    }

    private void initUIElements() {
        if(topic == null && rootTopics == null) {
            isRootTopics = true;
            khanAcademy.getRootTopics(new DataCallback<List<KhanTopic>>() {
                @Override
                public void receiveResults(List<KhanTopic> results) {
                    rootTopics = results;
                    decorateUIWithRootResultList(rootTopics);
                }
            });
        }
        else {
            createResultList();
        }
    }

    private void extractOutState(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey("topic")) {
                topic = (KhanTopic) savedInstanceState.getSerializable("topic");
            }
            if(savedInstanceState.containsKey("is_root")) {
                isRootTopics = savedInstanceState.getBoolean("is_root");
            }
        }
    }

    private void extractArguments() {
        Bundle b = getArguments();
        if(b != null) {
            if(b.containsKey("topic")) {
                topic = (KhanTopic) getArguments().getSerializable("topic");
            }
        }
    }

    private void createResultList() {
        KhanTopicFactory khanTopicFactory = new KhanTopicFactory();
        if(topic != null) {
            List<? extends TopicData> children = topic.getChildren();
            rootTopics = new ArrayList<KhanTopic>();
            for(TopicData topicData : children) {
                JsonData jsonData = (JsonData) topicData;
                KhanTopic khanTopic = khanTopicFactory.createTopicFromParentTopic(jsonData, topic);
                rootTopics.add(khanTopic);
            }
        }
        if(isRootTopics) {
            decorateUIWithResultList(rootTopics);
        }
        else {
            decorateUIWithResultList(rootTopics);
        }
    }

    private void decorateUIWithRootResultList(List<KhanTopic> results) {
        topicContainer.removeAllViews();
        for (KhanTopic topic : results) {
            View topicView = layoutInflater.inflate(R.layout.root_topic_item_layout, topicContainer, false);

            ((TextView) topicView.findViewById(R.id.textView_title)).setText(topic.getTopicData().getDisplayName());
            ((TextView) topicView.findViewById(R.id.textView_description)).setText(topic.getTopicData().getDescription());
            ((TextView) topicView.findViewById(R.id.textView_topicCount)).setText(topic.getChildren().size() + " topics");
            ((TextView) topicView.findViewById(R.id.textView_icon_letter)).setText(topic.getTopicData().getDisplayName().substring(0, 1).toUpperCase());
            ((GradientDrawable) topicView.findViewById(R.id.circle_shape).getBackground()).setColor(topic.getTopicData().getPrimaryColor());

            topicView.setTag(topic);
            topicView.setClickable(true);
            setLinearLayoutClickListener(topicView);
            topicContainer.addView(topicView);
        }
    }

    private void decorateUIWithResultList(List<KhanTopic> results) {
        topicContainer.removeAllViews();
        for (KhanTopic topic : results) {
            View v;
            if(topic.isVideoTopic()) {
                v = decorateViewAsVideoTopic(topic);
            }
            else {
                v = decorateDefaultTopicView(topic);
            }
            topicContainer.addView(v);
        }
    }

    private View decorateDefaultTopicView(Topic topic) {
        View topicView = layoutInflater.inflate(R.layout.topic_item_layout, topicContainer, false);

        ((TextView) topicView.findViewById(R.id.textView_title)).setText(topic.getTopicData().getDisplayName());
        ((TextView) topicView.findViewById(R.id.textView_description)).setText(topic.getTopicData().getDescription());
        ((TextView) topicView.findViewById(R.id.textView_icon_letter)).setText(topic.getTopicData().getDisplayName().substring(0, 1).toUpperCase());
        ((GradientDrawable) topicView.findViewById(R.id.circle_shape).getBackground()).setColor(topic.getTopicData().getPrimaryColor());

        topicView.setTag(topic);
        topicView.setClickable(true);
        setLinearLayoutClickListener(topicView);

        return topicView;
    }

    private View decorateViewAsVideoTopic(Topic topic) {
        View topicView = layoutInflater.inflate(R.layout.topic_video_item_layout, topicContainer, false);

        ((TextView) topicView.findViewById(R.id.textView_title)).setText(topic.getTopicData().getDisplayName());
        ((TextView) topicView.findViewById(R.id.textView_description)).setText(topic.getTopicData().getDescription());
        ((GradientDrawable) topicView.findViewById(R.id.imageView_video_icon).getBackground()).setColor(topic.getTopicData().getPrimaryVideoColor());
//            ((TextView) topicView.findViewById(R.id.textView_topicCount)).setText(topic.getChildren().size() + " topics");
        topicView.setTag(topic);
        topicView.setClickable(true);
        setLinearLayoutClickListener(topicView);

        return topicView;
    }

    private void setLinearLayoutClickListener(View v) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Topic t = (Topic) v.getTag();
                Log.d(getClass().getName(), "Clicked topic: " + t.getTopicData().getDisplayName());
                handleTopicClick(t);
            }
        });
    }

    private void handleTopicClick(Topic clickedTopic) {
        if(clickedTopic.getKind().equalsIgnoreCase("video")) {
            YouTubeVideoFragment frag = new YouTubeVideoFragment();
            Bundle args = new Bundle();
            args.putSerializable("topic", clickedTopic);
            frag.setArguments(args);
            switchFragment(frag);
        }
        else {
            khanAcademy.getTopicByID(clickedTopic.getId(), new DataCallback<KhanTopic>() {
                @Override
                public void receiveResults(KhanTopic results) {
                    KhanAcademyTopicBrowserFragment frag = new KhanAcademyTopicBrowserFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("topic", results);
                    frag.setArguments(args);
                    switchFragment(frag);
                }
            });
        }
    }

    private void switchFragment(Fragment newFrag) {
        final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, newFrag)
                .addToBackStack(null)
                .commit();
    }

    private void initKhanService() {
        if(khanAcademy == null) {
            khanAcademy = new KhanAcademy(new SimpleClientCallback() {
                @Override
                public void onServiceConnected() {
                    if(topic == null && rootTopics == null) {
                        initUIElements();
                    }
                }

                @Override
                public void handleServiceMessage(Message msg) {
                    Log.d(getClass().getName(), "GOT SERVICE MESSAGE!");
                }
            });
        }
    }

    private void handleConfigChange(Configuration newConfig) {

    }

    private void handleSaveInstanceState(Bundle outState) {
        if(topic != null) {
            outState.putSerializable("topic", topic);
        }
        outState.putBoolean("is_root", isRootTopics);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        handleSaveInstanceState(outState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        handleConfigChange(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

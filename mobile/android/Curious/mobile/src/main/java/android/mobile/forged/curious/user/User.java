package android.mobile.forged.curious.user;

import android.mobile.forged.curious.data.UserData;

/**
 * Created by visitor15 on 3/1/15.
 */
public interface User {

    public String getDisplayName();

    public String getUsername();

    public String getEmail();

    public UserData getUserData();
}

package android.mobile.forged.curious.data;


import java.io.Serializable;

/**
 * Created by visitor15 on 2/18/15.
 */
public interface TopicData extends Serializable {
    public String getDisplayName();

    public String getDescription();

    public int getPrimaryColor();

    public int getSecondaryColor();

    public int getBackgroundColor();

    public int getPrimaryVideoColor();

    public void setPrimaryColor(final int colorResource);

    public void setSecondaryColor(final int colorResource);

    public void setBackgroundColor(final int colorResource);

    public void setPrimaryVideoColor(final int colorResource);

    public void setDescription(String description);

    public String getDomain();

    public void setDomain(String domain);

    public void setParentDomain(String domain);
}

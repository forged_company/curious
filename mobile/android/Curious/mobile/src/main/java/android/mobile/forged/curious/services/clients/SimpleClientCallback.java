package android.mobile.forged.curious.services.clients;

import android.os.Message;

import java.io.Serializable;

/**
 * Created by visitor15 on 2/19/15.
 */
public interface  SimpleClientCallback extends Serializable {
    public void onServiceConnected();

    public void handleServiceMessage(Message msg);
}
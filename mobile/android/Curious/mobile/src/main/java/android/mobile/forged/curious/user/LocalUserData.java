package android.mobile.forged.curious.user;

import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.UserData;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

/**
 * Created by visitor15 on 3/3/15.
 */
public abstract class LocalUserData<T> extends JsonData<T> implements UserData {

    @SerializedName("user_name")
    private String user_name = "";

    @SerializedName("display_name")
    private String display_name = "";

    @SerializedName("email")
    private String email = "";

    public LocalUserData(String jsonSource, String jsonString) {
        super(jsonSource, jsonString);
    }

    public LocalUserData(String jsonData) {
        super(jsonData);
    }

    @Override
    public Optional<T> resolve() {
        T element;
        Gson gson = new GsonBuilder().create();
        try {
            element = gson.fromJson(getJsonString(), new TypeToken<LocalUserData>() {
            }.getType());
        } catch (NullPointerException e) {
            element = (T) this;
        }
        return Optional.fromNullable(element);
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(LocalUserData.this, new TypeToken<LocalUserData>() {
        }.getType());
    }
}

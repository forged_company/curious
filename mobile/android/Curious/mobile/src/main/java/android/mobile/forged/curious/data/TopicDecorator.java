package android.mobile.forged.curious.data;

import android.mobile.forged.curious.entities.Topic;

/**
 * Created by visitor15 on 2/19/15.
 */
public interface TopicDecorator {

    public Topic decorateTopic(Topic topic);
}

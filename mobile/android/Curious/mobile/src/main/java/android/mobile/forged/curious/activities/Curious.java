package android.mobile.forged.curious.activities;

import android.app.Application;
import android.mobile.forged.curious.integrated.khan_academy.auth.KhanAcademyAuthenticator;
import android.mobile.forged.curious.integrated.khan_academy.storage.KhanDBHelper;
import android.mobile.forged.curious.integrated.khan_academy.storage.KhanDBManager;
import android.mobile.forged.curious.services.AuthService;
import android.mobile.forged.curious.services.UserService;
import android.mobile.forged.curious.storage.DBManager;
import android.mobile.forged.curious.storage.DBManagerHelper;
import android.mobile.forged.curious.storage.LRUCacheManager;
import android.mobile.forged.curious.storage.SimpleDatabaseManager;
import android.mobile.forged.curious.user.User;

/**
 * Created by visitor15 on 2/17/15.
 */
public class Curious extends Application {
    private static Curious singleton;

    private static LRUCacheManager cacheManager;

    private static SimpleDatabaseManager khanDBManager;

    private static SimpleDatabaseManager defaultDBManager;

    private static UserService userService;

    private static AuthService authService;

    private static User currentUser;

    @Override
    public void onCreate() {
        Curious.singleton = this;
        cacheManager = new LRUCacheManager();
        khanDBManager = new KhanDBManager(this, KhanDBHelper.DB_NAME, null, KhanDBHelper.DB_VERSION);
        defaultDBManager = new DBManager(this, DBManagerHelper.DB_NAME, null, DBManagerHelper.DB_VERSION);
        userService = new UserService();
        authService = new KhanAcademyAuthenticator();
        super.onCreate();
    }

    public static Curious getContext() {
        return Curious.singleton;
    }

    public static LRUCacheManager getCacheManager() { return Curious.cacheManager; }

    public static SimpleDatabaseManager getKhanDBManager() { return khanDBManager; }

    public static SimpleDatabaseManager getDefaultDatabase() { return defaultDBManager; }

    public static UserService getUserService() { return userService; }

    public static AuthService getAuthService() { return authService; }

    public static User getCurrentUser() {
        return currentUser;
    }
}

package android.mobile.forged.curious.integrated.khan_academy.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicDataBuilder;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanUserData;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.storage.SimpleDatabaseManager;
import android.mobile.forged.curious.user.LocalUserData;
import android.mobile.forged.curious.user.User;
import android.util.Log;

import com.google.common.base.Optional;

import java.util.Map;

/**
 * Created by visitor15 on 2/17/15.
 */
public class KhanDBManager extends SimpleDatabaseManager {

    /**
     * Create a helper object to create, open, and/or manage a database.
     * This method always returns very quickly.  The database is not actually
     * created or opened until one of {@link #getWritableDatabase} or
     * {@link #getReadableDatabase} is called.
     *
     * @param context to use to open or create the database
     * @param name    of the database file, or null for an in-memory database
     * @param factory to use for creating cursor objects, or null for the default
     * @param version number of the database (starting at 1); if the database is older,
     *                {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                newer, {@link #onDowngrade} will be used to downgrade the database
     */
    public KhanDBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Create a helper object to create, open, and/or manage a database.
     * The database is not actually created or opened until one of
     * {@link #getWritableDatabase} or {@link #getReadableDatabase} is called.
     * <p/>
     * <p>Accepts input param: a concrete instance of {@link android.database.DatabaseErrorHandler} to be
     * used to handle corruption when sqlite reports database corruption.</p>
     *
     * @param context      to use to open or create the database
     * @param name         of the database file, or null for an in-memory database
     * @param factory      to use for creating cursor objects, or null for the default
     * @param version      number of the database (starting at 1); if the database is older,
     *                     {@link #onUpgrade} will be used to upgrade the database; if the database is
     *                     newer, {@link #onDowngrade} will be used to downgrade the database
     * @param errorHandler the {@link android.database.DatabaseErrorHandler} to be used when sqlite reports database
     */
    public KhanDBManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        createDatabase(db, KhanDBHelper.DB_VERSION);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        upgradeDatabase(db, newVersion);
    }

    @Override
    protected void createDatabase(SQLiteDatabase db, int version) {
        db.execSQL(KhanDBHelper.CREATE_DEFAULT_DATABASE);
        db.execSQL(KhanDBHelper.CREATE_AUTH_DATABASE);
        db.execSQL(KhanDBHelper.CREATE_USER_TABLE);
    }

    @Override
    protected void upgradeDatabase(SQLiteDatabase db, int newVersion) {

    }

    @Override
    public long persistTopic(Topic topic) {
        return createTopicEntry(topic);
    }

    @Override
    public long persistUser(User user) {
        return persistUserInternal(user);
    }

    @Override
    public Topic findTopicByTopicID(final String topicId) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_TOPIC_ID,
                KhanDBHelper.DBEntries.COLUMN_JSON_DATA
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_DISPLAY_TITLE + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_TOPIC_ID + " = ?";
        String[] selectionArgs = { topicId };
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_TOPIC,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            String jsonData = c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_JSON_DATA));

            KhanTopicDataBuilder dataTopicBuilder = new KhanTopicDataBuilder();
            KhanTopicFactory topicFactory = new KhanTopicFactory();
            return topicFactory.createTopic(dataTopicBuilder.buildFromJsonString(jsonData));
        }
        return null;
    }

    @Override
    public long persistAuthentication(User currentUser, Map<String, String> authResults) {
        // TODO This entire strategy needs to be refactored.
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KhanDBHelper.DBEntries.COLUMN_USERNAME, "nickc.dev");
        values.put(KhanDBHelper.DBEntries.COLUMN_USERDATA, "");

        if(authResults.containsKey("oauth_refresh_token")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN, authResults.get("oauth_refresh_token"));
        }
        if(authResults.containsKey("oauth_verifier")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_OAUTH_VERIFIER, authResults.get("oauth_verifier"));
        }
        if(authResults.containsKey("oauth_token")) {
            String results = authResults.get("oauth_token");
            values.put(KhanDBHelper.DBEntries.COLUMN_TOKEN, results);
        }
        if(authResults.containsKey("oauth_token_secret")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_TOKEN_SECRET, authResults.get("oauth_token_secret"));
        }
        if(authResults.containsKey("oauth_refresh_token_secret")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN_SECRET, authResults.get("oauth_refresh_token_secret"));
        }

        boolean newUser = !isExistingUser(currentUser);

        if(newUser) {
            // Insert the new row, returning the primary key value of the new row
            return db.insert(
                    KhanDBHelper.DBEntries.TABLE_NAME_AUTH,
                    null,
                    values);
        }
        else {
            return updateCurrentUserAuthCredentials(currentUser, authResults);
        }
    }

    private boolean isExistingUser(final User user) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_USERNAME
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = {"nickc.dev"};
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return c.moveToFirst();
    }

    private long updateCurrentUserAuthCredentials(final User currentUser, final Map<String, String> authResults) {
        long val = -1;

        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KhanDBHelper.DBEntries.COLUMN_USERNAME, "nickc.dev");
        values.put(KhanDBHelper.DBEntries.COLUMN_USERDATA, "");

        if(authResults.containsKey("oauth_refresh_token")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN, authResults.get("oauth_refresh_token"));
        }
        if(authResults.containsKey("oauth_verifier")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_OAUTH_VERIFIER, authResults.get("oauth_verifier"));
        }
        if(authResults.containsKey("oauth_token")) {
            String results = authResults.get("oauth_token");
            values.put(KhanDBHelper.DBEntries.COLUMN_TOKEN, results);
        }
        if(authResults.containsKey("oauth_token_secret")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_TOKEN_SECRET, authResults.get("oauth_token_secret"));
        }
        if(authResults.containsKey("oauth_refresh_token_secret")) {
            values.put(KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN_SECRET, authResults.get("oauth_refresh_token_secret"));
        }

        val = db.update(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,
                values,
                KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?",
                new String[]{"nickc.dev"});

        return val;
    }

    private long updatePersistedUser(final User user) {
        long val = -1;

        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KhanDBHelper.DBEntries.COLUMN_USERNAME, user.getUsername());
        values.put(KhanDBHelper.DBEntries.COLUMN_USERDATA, "");
        val = db.update(
                KhanDBHelper.DBEntries.TABLE_NAME_USERS,
                values,
                KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?",
                new String[]{user.getUsername()});

        return val;
    }

    private long persistOauthVerifier(User currentUser, Map<String, String> authResults) {
        return -1;
    }

    @Override
    public String getRefreshTokenForUser(User currentUser) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN,
                KhanDBHelper.DBEntries.COLUMN_USERNAME
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { "nickc.dev" };
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            return c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN));
        }
        return "";
    }

    @Override
    public String getOauthVerifierForUser(User currentUser) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_OAUTH_VERIFIER,
                KhanDBHelper.DBEntries.COLUMN_USERNAME
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { "nickc.dev" };
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            return c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_OAUTH_VERIFIER));
        }
        return "";
    }

    @Override
    public String getSignatureSigningKey(User currentUser) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN_SECRET,
                KhanDBHelper.DBEntries.COLUMN_USERNAME
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { "nickc.dev" };
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            return c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_REFRESH_TOKEN_SECRET));
        }
        return "";
    }

    @Override
    public String getAccessTokenForUser(User currentUser) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_TOKEN,
                KhanDBHelper.DBEntries.COLUMN_USERNAME
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { "nickc.dev" };
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            String data = c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_TOKEN));
            Log.d("TAG", "GOT DB DATA: " + data);
            return data;
        }
        return "";
    }

    @Override
    public Optional<KhanAcademyUser> getUser(String userName) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_USERNAME,
                KhanDBHelper.DBEntries.COLUMN_USERDATA
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = {"nickc.dev"};
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            String userStringData = c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_USERDATA));
            KhanUserData khanUserData = new KhanUserData(userStringData);
            Optional<KhanUserData> userData = khanUserData.resolve();
            if(userData.isPresent()) {
                return Optional.of(new KhanAcademyUser(userData.get()));
            }
        }
        return Optional.absent();
    }

    @Override
    public String getAccessTokenSignatureSigningKey(User currentUser) {
        SQLiteDatabase db = getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                KhanDBHelper.DBEntries._ID,
                KhanDBHelper.DBEntries.COLUMN_TOKEN_SECRET,
                KhanDBHelper.DBEntries.COLUMN_USERNAME
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = KhanDBHelper.DBEntries.COLUMN_USERNAME + " DESC";

        String selection = KhanDBHelper.DBEntries.COLUMN_USERNAME + " = ?";
        String[] selectionArgs = { "nickc.dev" };
        Cursor c = db.query(
                KhanDBHelper.DBEntries.TABLE_NAME_AUTH,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if(c.moveToFirst()) {
            return c.getString(c.getColumnIndexOrThrow(KhanDBHelper.DBEntries.COLUMN_TOKEN_SECRET));
        }
        return "";
    }

    public long createTopicEntry(final Topic topic) {
        // Gets the data repository in write mode
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        // TODO video topics are returning a number as an idea. This needs to be handled.
        values.put(KhanDBHelper.DBEntries.COLUMN_TOPIC_ID, topic.getId());
        values.put(KhanDBHelper.DBEntries.COLUMN_DISPLAY_TITLE, topic.getTopicData().getDisplayName());
        values.put(KhanDBHelper.DBEntries.COLUMN_DOMAIN_SLUG, topic.getTopicData().getDomain());
        values.put(KhanDBHelper.DBEntries.COLUMN_PARENT_DOMAIN, topic.getTopicData().getDomain());
        values.put(KhanDBHelper.DBEntries.COLUMN_TOPIC_TYPE, topic.getKind());
        values.put(KhanDBHelper.DBEntries.COLUMN_JSON_DATA, topic.getTopicData().toString());

        // Insert the new row, returning the primary key value of the new row
        return db.insert(
                KhanDBHelper.DBEntries.TABLE_NAME_TOPIC,
                null,
                values);
    }

    public long persistUserInternal(final User user) {
        if(!isExistingUser(user)) {
            // Gets the data repository in write mode
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            // TODO video topics are returning a number as an idea. This needs to be handled.
            values.put(KhanDBHelper.DBEntries.COLUMN_USERNAME, user.getUsername());
            values.put(KhanDBHelper.DBEntries.COLUMN_USERDATA, user.getUserData().toString());

            // Insert the new row, returning the primary key value of the new row
            return db.insert(
                    KhanDBHelper.DBEntries.TABLE_NAME_TOPIC,
                    null,
                    values);
        }
        else {
            return updatePersistedUser(user);
        }
    }
}




























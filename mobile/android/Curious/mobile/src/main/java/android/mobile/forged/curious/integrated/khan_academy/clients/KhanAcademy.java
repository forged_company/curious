package android.mobile.forged.curious.integrated.khan_academy.clients;

import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanUserData;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;
import android.mobile.forged.curious.integrated.khan_academy.services.KhanService;
import android.mobile.forged.curious.integrated.khan_academy.user.KhanAcademyUser;
import android.mobile.forged.curious.services.clients.LocalServiceClient;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by visitor15 on 2/19/15.
 */
public class KhanAcademy extends LocalServiceClient implements KhanAcademyClient {

    public KhanAcademy() { super(); }

    @Override
    protected void onHandleMessage(Message msg) {
        switch (msg.what) {
            case KhanService.Services.REQUEST_ROOT_TOPICS: {
                handleRootTopicRequest(msg.getData());
                break;
            }
            case KhanService.Services.REQUEST_USER: {
                handleRequestUser(msg.getData());
                break;
            }
            default: {
                handleTopicRequest(msg.getData());
            }
        }
    }

    private void handleRequestUser(final Bundle b) {
        if (b != null) {
            if (b.containsKey("callback")) {
                if (b.containsKey("result_set")) {
                    Bundle resultSet = b.getBundle("result_set");
                    if (resultSet.containsKey("result_set")) {
                        String jsonData = resultSet.getString("result_set");
                        KhanUserData userData = new KhanUserData(jsonData);
                        KhanAcademyUser user = new KhanAcademyUser(userData.resolve().get());
                        ((DataCallback) b.getSerializable("callback")).receiveResults(user);
                    }
                }
            }
        }
    }

    private void persistUser(final KhanAcademyUser user) {
        Curious.getKhanDBManager().persistUser(user);
    }

    private void handleTopicRequest(final Bundle b) {
        if (b != null) {
            if (b.containsKey("callback")) {
                if (b.containsKey("result_set")) {
                    Bundle resultSet = b.getBundle("result_set");
                    if (resultSet.containsKey("result_set")) {
                        JsonData jsonData = (JsonData) resultSet.getSerializable("result_set");
                        KhanTopicFactory khanTopicFactory = new KhanTopicFactory();
                        Topic t = khanTopicFactory.createTopic(jsonData);
                        ((DataCallback) b.getSerializable("callback")).receiveResults(t);
                    }
                }
            }
        }
    }

    private void handleRootTopicRequest(final Bundle b) {
        if (b != null) {
            List<Topic> topics = new ArrayList<Topic>();
            if (b.containsKey("callback")) {
                if (b.containsKey("result_set")) {
                    Bundle resultSet = b.getBundle("result_set");
                    Set<String> keys = resultSet.keySet();
                    for(String key : keys) {
                        Bundle resultBundle = (Bundle) resultSet.getBundle(key);
                        if(resultBundle.containsKey("result_set")) {
                            JsonData jsonData = (JsonData) resultBundle.getSerializable("result_set");
                            KhanTopicFactory khanTopicFactory = new KhanTopicFactory();
                            Topic t = khanTopicFactory.createTopic(jsonData);
                            topics.add(t);
                        }
                    }
                }
                ((DataCallback) b.getSerializable("callback")).receiveResults(topics);
            }
        }
    }

    public KhanAcademy(SimpleClientCallback callback) {
        super(callback, KhanService.class);
    }

    private void sendMessage(int requestCommand, DataCallback c) {
        try {
            Message msg = Message.obtain();
            msg.what = requestCommand;

            Bundle b = new Bundle();
            b.putSerializable("callback", c);
            msg.setData(b);
            msg.replyTo = _mCallbackMessenger;

            _messenger.send(msg);
        } catch (Exception e) {
        }
    }

    private void sendMessage(int requestCommand, Bundle b, DataCallback c) {
        try {
            Message msg = Message.obtain();
            msg.what = requestCommand;
            b.putSerializable("callback", c);
            msg.setData(b);
            msg.replyTo = _mCallbackMessenger;

            _messenger.send(msg);
        } catch (Exception e) {
        }
    }

    private void sendMessageForListResults(int requestCommand, DataCallback<List<KhanTopic>> callback) {
        try {
            Message msg = Message.obtain();
            msg.what = requestCommand;

            Bundle b = new Bundle();
            b.putSerializable("callback", callback);
            msg.setData(b);
            msg.replyTo = _mCallbackMessenger;

            _messenger.send(msg);
        } catch (Exception e) {
        }
    }

    private void sendMessage(int requestCommand, String topicId, DataCallback<? extends Topic> c) {
        try {
            Message msg = Message.obtain();
            msg.what = requestCommand;

            Bundle b = new Bundle();
            b.putSerializable("callback", c);
            b.putString("topic_id", topicId);
            msg.setData(b);
            msg.replyTo = _mCallbackMessenger;

            _messenger.send(msg);
        } catch (Exception e) {
        }
    }

    @Override
    public void getNewAndNoteworthyTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_NEW_AND_NOTEWORTHY_TOPICS, c);
    }

    @Override
    public void getAllMathTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_MATH_TOPICS, c);
    }

    @Override
    public void getAllScienceTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_SCIENCE_TOPICS, c);
    }

    @Override
    public void getAllEconomicsAndFinanceTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_ECONOMICS_AND_FINANCE_TOPICS, c);
    }

    @Override
    public void getAllArtsAndHumanitiesTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_ARTS_AND_HUMANITIES_TOPICS, c);
    }

    @Override
    public void getAllComputingTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_COMPUTING_TOPICS, c);
    }

    @Override
    public void getAllTestPrepTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_TEST_PREP_TOPICS, c);
    }

    @Override
    public void getAllPartnerContentTopics(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_PARTNER_CONTENT_TOPICS, c);
    }

    @Override
    public void getAllTalksAndInterviews(DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_TALKS_AND_INTERVIEW_TOPICS, c);
    }

    @Override
    public void getRootTopics(DataCallback<List<KhanTopic>> c) {
        sendMessageForListResults(KhanService.Services.REQUEST_ROOT_TOPICS, c);
    }

    @Override
    public void getUser(final String userName, DataCallback<KhanAcademyUser> c) {
        Bundle b = new Bundle();
        b.putString("user_name", userName);
        sendMessage(KhanService.Services.REQUEST_USER, b, c);
    }

    public void getTopicByID(String topicId, DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_TOPIC_BY_TOPIC_ID, topicId, c);
    }

    public void getVideoDataForTopic(KhanTopic khanTopic, DataCallback<KhanTopic> c) {
        sendMessage(KhanService.Services.REQUEST_VIDEO_DATA_BY_TOPIC_ID, khanTopic.getId(), c);
    }
}

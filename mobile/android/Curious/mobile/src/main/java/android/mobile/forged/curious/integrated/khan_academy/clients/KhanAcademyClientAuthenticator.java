package android.mobile.forged.curious.integrated.khan_academy.clients;

import android.mobile.forged.curious.data.DataCallback;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.auth.KhanAcademyAuthenticator;
import android.mobile.forged.curious.services.AuthService;
import android.mobile.forged.curious.services.clients.LocalServiceClient;
import android.mobile.forged.curious.services.clients.SimpleClientCallback;
import android.mobile.forged.curious.user.User;
import android.os.Bundle;
import android.os.Message;

/**
 * Created by visitor15 on 3/2/15.
 */
public class KhanAcademyClientAuthenticator extends LocalServiceClient {

    private SimpleClientCallback internalCallback;

    public KhanAcademyClientAuthenticator(SimpleClientCallback callback) {
        super(callback, KhanAcademyAuthenticator.class);
    }

    @Override
    protected void onHandleMessage(Message msg) {
        switch(msg.what) {
            default: {
                Bundle b = msg.getData();
                if(b.containsKey("result_set")) {
                    Bundle resultSet = b.getBundle("result_set");
                    ((DataCallback) b.getSerializable("callback")).receiveResults(resultSet);
                }
                break;
            }
        }
    }

    private void handleOnServiceConnectedInternal() {

    }

    private void handleServiceMessageInternal(Message msg) {

    }

    private void sendMessage(int requestCommand, DataCallback c) {
        try {
            Message msg = Message.obtain();
            msg.what = requestCommand;

            Bundle b = new Bundle();
            b.putSerializable("callback", c);
            msg.setData(b);
            msg.replyTo = _mCallbackMessenger;

            _messenger.send(msg);
        } catch (Exception e) {
        }
    }

    private void initInternalSimpleCallback() {
        internalCallback = new SimpleClientCallback() {
            @Override
            public void onServiceConnected() {
                handleOnServiceConnectedInternal();
            }

            @Override
            public void handleServiceMessage(Message msg) {
                handleServiceMessageInternal(Message.obtain(msg));
            }
        };
    }

    public void authenticateUser(User user, DataCallback<Bundle> callback) {
        sendMessage(AuthService.Services.REQUEST_AUTHENTICATION, callback);
    }
}
































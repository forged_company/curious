package android.mobile.forged.curious.data;


import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.entities.Topic;
import android.mobile.forged.curious.integrated.khan_academy.data.KhanTopicData;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;

import com.google.common.base.Optional;

/**
 * Created by visitor15 on 2/24/15.
 */
public class TopicReference implements SimpleReference {

    private final String topicId;

    public TopicReference(final Topic t) {
        topicId = t.getId();
    }

    @Override
    public Optional<Topic> resolve() {
        Optional<TopicData> referencedTopic = Curious.getCacheManager().getCachedTopic(topicId);
        if(referencedTopic.isPresent()) {
            return Optional.of((Topic) new KhanTopic((KhanTopicData) referencedTopic.get()));
        }
        return Optional.absent();
    }
}

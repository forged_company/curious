package android.mobile.forged.curious.integrated.khan_academy.data;

import android.mobile.forged.curious.R;
import android.mobile.forged.curious.activities.Curious;
import android.mobile.forged.curious.data.JsonData;
import android.mobile.forged.curious.data.TopicFactory;
import android.mobile.forged.curious.integrated.khan_academy.entities.KhanTopic;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visitor15 on 2/18/15.
 *
 * Class: Used to produce decorated Topics.
 */
public class KhanTopicFactory implements TopicFactory<KhanTopic> {

    public KhanTopicFactory() {}

    @Override
    public KhanTopic createTopic(JsonData data) {
        return filterForOnlyVideosAndTopics(createTopicInternal(data));
    }

    public KhanTopic createTopicInternal(final JsonData data) {
        KhanTopicData t;
        try {
            Optional<KhanTopicData> optTopic = data.resolve();
            t = (optTopic.isPresent()) ? optTopic.get() : null;

            if(t.getDomain() == null) {
                t.setDomain("");
            }

            return beginDecoration(t);
        } catch(NullPointerException e) {
            return beginDecoration(data);
        }
    }

    public KhanTopic createTopicFromParentTopic(final JsonData data, final KhanTopic parentTopic) {
        KhanTopicData t;
        try {
            Optional<KhanTopicData> optTopic = data.resolve();
            t = (optTopic.isPresent()) ? optTopic.get() : null;
            if(t != null && Strings.isNullOrEmpty(t.getDomain())) {
                t.setParentDomain(parentTopic.getTopicData().getDomain());
            }
            return beginDecoration(t);
        } catch(NullPointerException e) {
            return beginDecoration(data);
        }
    }

    private KhanTopic beginDecoration(final JsonData topicData) {
        return decorateTopic(new KhanTopic((KhanTopicData) topicData));
    }

    // A topic is tailored to its domain.
    private KhanTopic decorateTopic(KhanTopic t) {
        if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.ARTS_AND_HUMANITIES) ||
                t.getUrl().contains(KhanTopic.TopicIDs.ARTS_AND_HUMANITIES)) {
            return createArtsAndHumanitiesTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.COLLEGE_ADMISSION) ||
                t.getUrl().contains(KhanTopic.TopicIDs.COLLEGE_ADMISSION)) {
            return createCollegeAdmissionsTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.COMPUTING) ||
                t.getUrl().contains(KhanTopic.TopicIDs.COMPUTING)) {
            return createComputingTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.ECONOMICS_AND_FINANCE) ||
                t.getUrl().equalsIgnoreCase(KhanTopic.TopicIDs.ECONOMICS_AND_FINANCE)) {
            return createEconomicsAndFinanceTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.MATH) ||
                t.getUrl().contains(KhanTopic.TopicIDs.MATH)) {
            return createMathTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.NEW_AND_NOTEWORY) ||
                t.getUrl().contains(KhanTopic.TopicIDs.NEW_AND_NOTEWORY)) {
            return createNewAndNoteworthyTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.PARTNER_CONTENT) ||
                t.getUrl().contains(KhanTopic.TopicIDs.PARTNER_CONTENT)) {
            return createPartnerContentTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.SCIENCE) ||
                t.getUrl().contains(KhanTopic.TopicIDs.SCIENCE)) {
            return createScienceTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.TALKS_AND_INTERVIEWS) ||
                t.getUrl().contains(KhanTopic.TopicIDs.TALKS_AND_INTERVIEWS)) {
            return createTalksAndInterviewsTopic(t);
        }
        else if(t.getTopicData().getDomain().equalsIgnoreCase(KhanTopic.TopicIDs.TEST_PREP) ||
                t.getUrl().contains(KhanTopic.TopicIDs.TEST_PREP)) {
            return createTestPrepTopic(t);
        }
        else {
            return createDefaultTopic(t);
        }
    }

    private KhanTopic filterForOnlyVideosAndTopics(KhanTopic topic) {
        List<KhanTopicData> subTopics = (List<KhanTopicData>) topic.getChildren();
        if(subTopics != null) {
            ArrayList<KhanTopicData> filteredList = new ArrayList<KhanTopicData>();
            for(KhanTopicData data : subTopics) {
                String topicKind = data.kind;
                if(!topicKind.equalsIgnoreCase("video") &&
                        !topicKind.equalsIgnoreCase("topic")) {
                    filteredList.add(data);
                }
            }
            subTopics.removeAll(filteredList);
        }
        return topic;
    }

    private KhanTopic setTopicColors(KhanTopic topic,
                                     int backgroundColor,
                                     int primaryColor,
                                     int secondaryColor,
                                     int primaryVideoColor) {
        topic.getTopicData().setBackgroundColor(Curious.getContext().getResources().getColor(backgroundColor));
        topic.getTopicData().setPrimaryColor(Curious.getContext().getResources().getColor(primaryColor));
        topic.getTopicData().setPrimaryVideoColor(Curious.getContext().getResources().getColor(primaryVideoColor));
        topic.getTopicData().setSecondaryColor(Curious.getContext().getResources().getColor(secondaryColor));
        return topic;
    }

    private KhanTopic createDefaultTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_default_background_color,
                R.color.topic_default_primary_color,
                R.color.topic_default_secondary_color,
                R.color.topic_default_video_primary_color);

        return t;
    }

    private KhanTopic createMathTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_math_background_color,
                R.color.topic_math_primary_color,
                R.color.topic_math_secondary_color,
                R.color.topic_math_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.math_desc));
        }
        return t;
    }

    private KhanTopic createScienceTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_science_background_color,
                R.color.topic_science_primary_color,
                R.color.topic_science_secondary_color,
                R.color.topic_science_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.science_desc));
        }
        return t;
    }

    private KhanTopic createNewAndNoteworthyTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_new_and_noteworthy_background_color,
                R.color.topic_new_and_noteworty_primary_color,
                R.color.topic_new_and_noteworthy_secondary_color,
                R.color.topic_new_and_noteworthy_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.new_and_noteworthy_desc));
        }
        return t;
    }

    private KhanTopic createComputingTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_computing_background_color,
                R.color.topic_computing_primary_color,
                R.color.topic_computing_secondary_color,
                R.color.topic_computing_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.computing_desc));
        }
        return t;
    }

    private KhanTopic createTalksAndInterviewsTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_talks_and_interviews_background_color,
                R.color.topic_talks_and_interviews_primary_color,
                R.color.topic_talks_and_interviews_secondary_color,
                R.color.topic_talks_and_interviews_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.talks_and_interviews_desc));
        }
        return t;
    }

    private KhanTopic createEconomicsAndFinanceTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_economics_finance_domain_background_color,
                R.color.topic_economics_finance_domain_primary_color,
                R.color.topic_economics_finance_domain_secondary_color,
                R.color.topic_economics_finance_domain_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.economics_finance_domain_desc));
        }
        return t;
    }

    private KhanTopic createTestPrepTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_test_prep_background_color,
                R.color.topic_test_prep_primary_color,
                R.color.topic_test_prep_secondary_color,
                R.color.topic_test_prep_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.test_prep_desc));
        }
        return t;
    }

    private KhanTopic createPartnerContentTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_partner_content_background_color,
                R.color.topic_partner_content_primary_color,
                R.color.topic_partner_content_secondary_color,
                R.color.topic_partner_content_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.partner_content_desc));
        }
        return t;
    }

    private KhanTopic createArtsAndHumanitiesTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_humanities_background_color,
                R.color.topic_humanities_primary_color,
                R.color.topic_humanities_secondary_color,
                R.color.topic_humanities_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.humanities_desc));
        }
        return t;
    }

    private KhanTopic createCollegeAdmissionsTopic(KhanTopic t) {
        t = setTopicColors(t,
                R.color.topic_college_admissions_background_color,
                R.color.topic_college_admissions_primary_color,
                R.color.topic_college_admissions_secondary_color,
                R.color.topic_college_admissions_video_primary_color);

        if(Strings.isNullOrEmpty(t.getTopicData().getDescription())) {
            t.getTopicData().setDescription(Curious.getContext().getResources().getString(R.string.college_admissions_desc));
        }
        return t;
    }
}

package android.mobile.forged.curious.storage;

import android.mobile.forged.curious.data.JsonData;

import com.google.common.base.Optional;

import java.util.List;

/**
 * Created by visitor15 on 2/21/15.
 */
public class CacheManager {

    // TODO this might be a singleton;
    public CacheManager() {

    }

    public Optional<List<JsonData>> getCachedResultsAsListForUrl(String url) {
        return Optional.absent();
    }

    public Optional<JsonData> getCachedResultsForUrl(String url) {
        return Optional.absent();
    }
}

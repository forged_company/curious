package android.mobile.forged.curious.auth;

import android.mobile.forged.curious.user.User;
import android.os.Bundle;

/**
 * Created by visitor15 on 3/1/15.
 */
public interface SimpleAuthenticator {

    public Bundle authenticate(final User user);
}

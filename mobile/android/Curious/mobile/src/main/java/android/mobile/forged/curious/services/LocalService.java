package android.mobile.forged.curious.services;

import android.app.Service;
import android.content.Intent;
import android.mobile.forged.curious.tasks.Task;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;

import java.util.Random;

/**
 * Created by visitor15 on 2/18/15.
 */
public abstract class LocalService extends Service {
    public static final int DO_TASK = 100;

    private Messenger mMmessenger;

    private Looper mLooper;

    private ServiceHandler mHandler;

    protected abstract void handleWorkerMessage(final Message msg);

    @Override
    public IBinder onBind(Intent intent) {
        return mMmessenger.getBinder();
    }

    @Override
    public void onCreate() {
        /* This is the service's main thread. */
        HandlerThread thread = new HandlerThread("LocalServiceThread", android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        /* Get the HandlerThread's Looper and use it for our handler. */
        mLooper = thread.getLooper();
        mHandler = new ServiceHandler(mLooper);
        mMmessenger = new Messenger(mHandler);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    private void handleServiceMessage(Message msg) {
        spawnWorkerThread().sendMessage(Message.obtain(msg));
//        switch(msg.what) {
//            case DO_TASK: {
//                spawnWorkerThread().sendMessage(Message.obtain(msg));
//                break;
//            }
//            default: {
//
//            }
//        }
    }

    protected void killCurrentWorkerThread() {
        if(Thread.currentThread() instanceof HandlerThread) {
            ((HandlerThread) Thread.currentThread()).quit();
        }
    }

    protected void doTask(Message msg) {
        ((Task) msg.getData().getSerializable("task")).execute();
        killCurrentWorkerThread();
    }

    private Handler spawnWorkerThread() {
        Random ran = new Random();
        HandlerThread thread = new HandlerThread("LocalServiceWorker" + ran.nextInt(100) + 1, android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        return new ServiceWorkerHandler(thread.getLooper());
    }

    /*
    * CLASS
    */
    protected class ServiceHandler extends Handler {

        protected ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            synchronized (this) {
                handleServiceMessage(Message.obtain(msg));
            }
        }
    }

    /*
    * CLASS
    */
    protected class ServiceWorkerHandler extends Handler {

        protected ServiceWorkerHandler(Looper looper) { super(looper); }

        @Override
        public void handleMessage(Message msg) {
            synchronized (this) {
                switch(msg.what) {
                    case Services.DO_TASK: {
                        doTask(Message.obtain(msg));
                        break;
                    }
                    default: {
                        handleWorkerMessage(Message.obtain(msg));
                    }
                }
            }
        }
    }

    public static final class Services {
        public static final int DO_TASK = 999;
    }
}
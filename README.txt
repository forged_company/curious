README

* Feature releases
* Agile-focused
* Never push to master

1.  Define feature.
2.  Split feature implementation into tasks.
3.  Create your feature branch from master.
4.  Branch from your feature branch to implement a task.
5.  Execute task.
6.  Test.
7.  Issue a pull request to the feature branch from your task-implementation branch.
8.  Test.
9.  Your pull request will be squashed to a single commit when it is merged into the feature branch. This should ease possible reverts hopefully.
10. Test.
11. Issue a pull request to the develop branch from your feature branch.